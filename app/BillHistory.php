<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillHistory extends Model
{
    protected $fillable = [
        'firstname', 'lastname', 'bill_type', 'amount', 'room_id', 'start_date', 'end_date', 'due_date'
    ];

    private function isDuplicateEntryException(QueryException $e)
    {
        $sqlState = $e->errorInfo[0];
        $errorCode  = $e->errorInfo[1];
        if ($sqlState === "23000" && $errorCode === 1062) {
          return true;
        }
        return false;
    }

}
