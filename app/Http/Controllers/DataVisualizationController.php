<?php

namespace App\Http\Controllers;

use App\GenInquiries;
use App\Billing;
use Session;
use Illuminate\Contracts\Validation\Rule;
use DB;
use Charts;
use App\Tenant;
use Illuminate\Http\Request;
use Carbon\Carbon;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class DataVisualizationController extends Controller
{
    public function index(Request $request) {

        $data = DB::table('tenants')
            ->select(
            DB::raw('sex as sex'),
            DB::raw('count(*) as number'))
            ->groupBy('sex')
            ->get();


            $array[] = ['Occupation', 'Number'];
            foreach($data as $key => $value){

            $array[++$key] = [$value->sex, $value->number];
            }


        $temps = array();
        $starts = substr(Carbon::now()->month(1)->toDateString(), 0, 7);
        $ends = substr(Carbon::now()->month(12)->toDateString(), 0, 7);
        while($starts <= $ends)
        {
            array_push($temps, array(
                "id" => Carbon::parse($starts)->format('M'), 
                "amount" => DB::table('tenants')->where('created_at', 'like', $starts . '%')->count()
            ));
            $starts = substr(Carbon::parse($starts)->month(Carbon::parse($starts)->month + 1)->toDateString(),0,7);
        }

        return view('vendor.voyager.datavisualizations.browse', compact('temps'))->with('sex', json_encode(

            $array));
    

      
    }
}
