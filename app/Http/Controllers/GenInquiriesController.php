<?php

namespace App\Http\Controllers;
use App\GenInquiries;
use App\Billing;
use Session;
use Illuminate\Contracts\Validation\Rule;
use DB;
use Charts;
use App\Tenant;
use Illuminate\Http\Request;
use Carbon\Carbon;

class GeninquiriesController extends Controller
{
    public function index()
    {   
        return view('welcome');

        
       
    }

  

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('contactus');
       

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $geninquiry = new GenInquiries;
      $geninquiry->name = $request->input('name');
      $geninquiry->email = $request->input('email');
      $geninquiry->message= $request->input('message');

      $token = $request->input('g-recaptcha-response');
    //   dd($token);
    //   $geninquiry->captcha= $request->input('captcha');
   
        if ($token) {

            $geninquiry->save();
            return redirect()->back()->with('alert', 'Your inquiry has been sent!');
        } else {

            return redirect('/contactus');
        }
      
    }


  

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

     /**
     * Get the validation error message.
     *
     * @return string
     */
    // public function message()
    // {
    //     return 'Are you a robot?';
    // }


}

