<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tenant;
use App\User;
use App\Message;
use Validator;
use Redirect;

class MessageController extends Controller
{
    public function addmessage(Request $req){
        $tenants = Tenant::all();
        $user = User::all();

        return view('createmessage')->withUsers($tenants->merge($user));
    }

    public function storemessage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'to' => 'required',
            'message'=> 'required',
        ]);

        if ($validator->fails()) {
        	\Session::flash('warnning','Please enter the valid details');
            return Redirect::to(route("messages.index"))->withInput()->withErrors($validator);
        }

        Message::create($request->all());
        return redirect(url('admin/messages'));
    }

    function index() {
        $messages = Message::where('to', auth()->user()->id)->get();
        // dd($messages);
        return view('vendor.voyager.messages.browse')->withMessages($messages);
    }
   
}