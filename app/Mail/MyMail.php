<?php

namespace App\Mail;

use App\BillHistory;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MyMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $details;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($details)
    {
        $this->details= $details;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $input = array(
            'firstname'     => $this->details['firstname'],
            'lastname'      => $this->details['lastname'],
            'middlename'    => $this->details['middlename'],
            'bill_type'     => $this->details['bill_type'],
            'start_date'    => $this->details['start_date'],
            'end_date'      => $this->details['end_date'],
            'due_date'      => $this->details['due_date'],
            'reading'       => $this->details['reading'],
            'amount'        => $this->details['amount'],
            'email'         => $this->details['email'],
        );

        return $this->from('feutechigniters@gmail.com')
                    ->view('email.mymail')
                    ->with([
                        'inputs' => $input,
                    ]);

        // return $this->view('emails.orders.shipped')
        // ->with([
        //     'orderName' => $this->order->name,
        //     'orderPrice' => $this->order->price,
        // ]);

        // return $this->from('universitypaypad.com')
        //             ->view('email.mymail')
        //             ->text('mails.demo_plain')
        //             ->with(
        //             [
        //                     'testVarOne' => '1',
        //                     'testVarTwo' => '2',
        //             ])
        //             ->attach(public_path('/images').'/demo.jpg', [
        //                     'as' => 'demo.jpg',
        //                     'mime' => 'image/jpeg',
        //             ]);
    }
}
