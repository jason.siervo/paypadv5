-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 03, 2018 at 10:08 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbpaypad`
--

-- --------------------------------------------------------

--
-- Table structure for table `announcements`
--

CREATE TABLE `announcements` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci,
  `user_id` int(11) DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `billings`
--

CREATE TABLE `billings` (
  `id` int(10) UNSIGNED NOT NULL,
  `amount` decimal(10,0) DEFAULT NULL,
  `bill_type` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `tenant_id` text COLLATE utf8mb4_unicode_ci,
  `room_id` int(11) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `reading` decimal(10,0) DEFAULT NULL,
  `due_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `billings`
--

INSERT INTO `billings` (`id`, `amount`, `bill_type`, `created_at`, `updated_at`, `tenant_id`, `room_id`, `start_date`, `end_date`, `reading`, `due_date`) VALUES
(110, '13000', '1', '2018-10-26 20:42:44', '2018-10-26 20:42:44', NULL, 8, '2018-10-03', '2018-10-17', '13000', '2018-10-25'),
(111, '14000', '2', '2018-10-26 20:43:52', '2018-10-26 20:43:52', NULL, 9, '2018-10-18', '2018-10-19', '14000', '2018-10-25'),
(112, '10000', '1', '2018-10-27 19:59:38', '2018-10-27 19:59:38', '14', NULL, '2018-10-03', '2018-10-04', '10000', '2018-10-05'),
(113, '10000', '1', '2018-10-27 20:01:04', '2018-10-27 20:01:04', '14', NULL, '2018-10-03', '2018-10-04', '10000', '2018-10-05'),
(114, '1000', '1', '2018-10-27 21:20:26', '2018-10-27 21:20:26', '14', NULL, '2018-10-02', '2018-10-03', '1000', '2018-10-04'),
(115, '1000', '1', '2018-10-27 21:30:25', '2018-10-27 21:30:25', '14', NULL, '2018-10-02', '2018-10-03', '1000', '2018-10-04'),
(116, '1000', '1', '2018-10-27 21:33:16', '2018-10-27 21:33:16', '16', NULL, '2018-10-03', '2018-10-04', '1000', '2018-10-05'),
(117, '2000', '1', '2018-10-27 21:39:12', '2018-10-27 21:39:12', NULL, 5, '2018-10-04', '2018-10-05', '2000', '2018-10-06'),
(118, '4000', '1', '2018-10-27 23:37:53', '2018-10-27 23:37:53', '15', NULL, '2018-10-03', '2018-10-04', '4000', '2018-10-05'),
(119, '5000', '1', '2018-10-27 23:43:07', '2018-10-27 23:43:07', '15', NULL, '2018-10-09', '2018-10-10', '5000', '2018-10-11'),
(120, '6', '1', '2018-10-27 23:50:58', '2018-10-27 23:50:58', '15', NULL, '2018-10-03', '2018-10-04', '6', '2018-10-05'),
(121, '6', '1', '2018-10-27 23:51:25', '2018-10-27 23:51:25', '15', NULL, '2018-10-03', '2018-10-04', '6', '2018-10-05'),
(122, '10', '1', '2018-10-27 23:55:00', '2018-10-27 23:55:00', '15', NULL, '2018-10-04', '2018-10-05', '10', '2018-10-06'),
(123, '8000', '2', '2018-10-27 23:57:49', '2018-10-27 23:57:49', '16', NULL, '2018-10-04', '2018-10-05', '8000', '2018-10-06'),
(124, '90000', '1', '2018-10-27 23:59:50', '2018-10-27 23:59:50', '15', NULL, '2018-10-04', '2018-10-05', '90000', '2018-10-06'),
(125, '3', '1', '2018-10-28 00:12:50', '2018-10-28 00:12:50', '15', NULL, '2018-10-23', '2018-10-24', '4', '2018-10-25'),
(126, '1000', '1', '2018-10-28 05:53:46', '2018-10-28 05:53:46', '14', 5, '2018-10-03', '2018-10-04', NULL, '2018-10-05'),
(127, '1000', '1', '2018-10-28 05:53:48', '2018-10-28 05:53:48', '14', 5, '2018-10-03', '2018-10-04', NULL, '2018-10-05'),
(128, '1000', '1', '2018-10-28 06:19:31', '2018-10-28 06:19:31', '14', 5, '2018-10-04', '2018-10-05', '1000', '2018-10-06'),
(129, '1000', '1', '2018-10-28 06:25:21', '2018-10-28 06:25:21', '14', 5, '2018-10-04', '2018-10-05', '1000', '2018-10-06'),
(130, '1111', '1', '2018-10-28 06:27:33', '2018-10-28 06:27:33', '17', 5, '2018-10-03', '2018-10-05', NULL, '2018-10-06'),
(131, '11111', '1', '2018-10-28 06:28:44', '2018-10-28 06:28:44', '14', 5, '2018-10-03', '2018-10-04', NULL, '2018-10-05'),
(132, '11111', '1', '2018-10-28 06:28:45', '2018-10-28 06:28:45', '14', 5, '2018-10-03', '2018-10-04', NULL, '2018-10-05'),
(133, '5', '1', '2018-10-28 07:28:16', '2018-10-28 07:28:16', '15', NULL, '2018-10-03', '2018-10-04', NULL, '2018-10-05'),
(134, '5', '1', '2018-10-28 07:28:35', '2018-10-28 07:28:35', '15', NULL, '2018-10-03', '2018-10-04', NULL, '2018-10-05'),
(135, '5', '1', '2018-10-28 07:30:51', '2018-10-28 07:30:51', '15', NULL, '2018-10-03', '2018-10-04', NULL, '2018-10-05'),
(136, '5', '1', '2018-10-28 07:31:22', '2018-10-28 07:31:22', '15', NULL, '2018-10-03', '2018-10-04', NULL, '2018-10-05'),
(137, '5', '1', '2018-10-28 07:32:18', '2018-10-28 07:32:18', '15', NULL, '2018-10-03', '2018-10-04', NULL, '2018-10-05'),
(138, '5', '1', '2018-10-28 07:34:41', '2018-10-28 07:34:41', '15', NULL, '2018-10-03', '2018-10-04', NULL, '2018-10-05'),
(139, '5', '1', '2018-10-28 07:50:28', '2018-10-28 07:50:28', '15', NULL, '2018-10-03', '2018-10-04', NULL, '2018-10-05'),
(140, '5', '1', '2018-10-28 07:51:17', '2018-10-28 07:51:17', '15', NULL, '2018-10-03', '2018-10-04', NULL, '2018-10-05'),
(141, '5', '1', '2018-10-28 07:52:04', '2018-10-28 07:52:04', '15', NULL, '2018-10-03', '2018-10-04', NULL, '2018-10-05'),
(142, '5', '1', '2018-10-28 07:59:06', '2018-10-28 07:59:06', '15', NULL, '2018-10-03', '2018-10-04', NULL, '2018-10-05'),
(143, '5', '1', '2018-10-28 08:03:40', '2018-10-28 08:03:40', '15', NULL, '2018-10-03', '2018-10-04', NULL, '2018-10-05'),
(144, '5', '1', '2018-10-28 08:40:37', '2018-10-28 08:40:37', '15', NULL, '2018-10-03', '2018-10-04', NULL, '2018-10-05'),
(145, '5', '1', '2018-10-28 08:42:35', '2018-10-28 08:42:35', '15', NULL, '2018-10-03', '2018-10-04', NULL, '2018-10-05'),
(146, '5', '1', '2018-10-28 09:01:52', '2018-10-28 09:01:52', '15', NULL, '2018-10-03', '2018-10-04', NULL, '2018-10-05'),
(147, '5', '1', '2018-10-28 09:06:14', '2018-10-28 09:06:14', '15', NULL, '2018-10-04', '2018-10-05', NULL, '2018-10-06'),
(148, '5', '1', '2018-10-28 09:07:16', '2018-10-28 09:07:16', '15', NULL, '2018-10-04', '2018-10-05', NULL, '2018-10-06'),
(149, '5', '1', '2018-10-28 10:06:24', '2018-10-28 10:06:24', '15', NULL, '2018-10-04', '2018-10-05', NULL, '2018-10-06'),
(150, '5', '1', '2018-10-28 10:37:01', '2018-10-28 10:37:01', '15', NULL, '2018-10-04', '2018-10-05', NULL, '2018-10-06'),
(151, '5', '1', '2018-10-28 10:39:00', '2018-10-28 10:39:00', '15', NULL, '2018-10-04', '2018-10-05', NULL, '2018-10-06'),
(152, '5', '1', '2018-10-28 10:40:38', '2018-10-28 10:40:38', '15', NULL, '2018-10-04', '2018-10-05', NULL, '2018-10-06'),
(153, '5', '1', '2018-10-28 10:42:40', '2018-10-28 10:42:40', '15', NULL, '2018-10-04', '2018-10-05', NULL, '2018-10-06'),
(154, '5', '1', '2018-10-28 10:43:27', '2018-10-28 10:43:27', '15', NULL, '2018-10-04', '2018-10-05', NULL, '2018-10-06'),
(155, '5', '1', '2018-10-28 10:44:39', '2018-10-28 10:44:39', '15', NULL, '2018-10-04', '2018-10-05', NULL, '2018-10-06'),
(156, '5', '1', '2018-10-28 10:45:10', '2018-10-28 10:45:10', '15', NULL, '2018-10-04', '2018-10-05', NULL, '2018-10-06'),
(157, '5', '1', '2018-10-28 10:46:34', '2018-10-28 10:46:34', '15', NULL, '2018-10-04', '2018-10-05', NULL, '2018-10-06'),
(158, '5', '1', '2018-10-28 10:47:16', '2018-10-28 10:47:16', '15', NULL, '2018-10-04', '2018-10-05', NULL, '2018-10-06'),
(159, '5', '1', '2018-10-28 10:47:37', '2018-10-28 10:47:37', '15', NULL, '2018-10-04', '2018-10-05', NULL, '2018-10-06'),
(160, '5', '1', '2018-10-28 10:49:21', '2018-10-28 10:49:21', '15', NULL, '2018-10-04', '2018-10-05', NULL, '2018-10-06'),
(161, '5', '1', '2018-10-28 10:50:46', '2018-10-28 10:50:46', '15', NULL, '2018-10-04', '2018-10-05', NULL, '2018-10-06'),
(162, '5', '1', '2018-10-28 10:51:48', '2018-10-28 10:51:48', '15', NULL, '2018-10-04', '2018-10-05', NULL, '2018-10-06'),
(163, '5', '1', '2018-10-28 10:54:17', '2018-10-28 10:54:17', '15', NULL, '2018-10-04', '2018-10-05', NULL, '2018-10-06'),
(164, '5', '1', '2018-10-28 10:55:41', '2018-10-28 10:55:41', '15', NULL, '2018-10-04', '2018-10-05', NULL, '2018-10-06'),
(165, '1000', '1', '2018-10-28 10:57:37', '2018-10-28 10:57:37', '15', NULL, '2018-10-03', '2018-10-04', '1000', '2018-10-05'),
(166, '5000', '1', '2018-10-28 11:50:29', '2018-10-28 11:50:29', '15', 7, '2018-10-03', '2018-10-04', '5000', '2018-10-05'),
(167, '5000', '1', '2018-10-28 11:51:06', '2018-10-28 11:51:06', '15', NULL, '2018-10-03', '2018-10-04', '5000', '2018-10-05'),
(168, '12345', '1', '2018-10-28 20:21:50', '2018-10-28 20:21:50', '14', NULL, '2018-10-30', '2018-11-03', '1', '2018-11-01'),
(169, '12345', '1', '2018-10-28 20:22:58', '2018-10-28 20:22:58', '14', NULL, '2018-10-30', '2018-11-03', '1', '2018-11-01'),
(170, '13000', '1', '2018-10-28 20:28:09', '2018-10-28 20:28:09', '15', NULL, '2018-10-29', '2018-10-30', '13000', '2018-10-31');

-- --------------------------------------------------------

--
-- Table structure for table `billing_categories`
--

CREATE TABLE `billing_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `bill_type` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `short_description` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `billing_categories`
--

INSERT INTO `billing_categories` (`id`, `bill_type`, `created_at`, `updated_at`, `short_description`) VALUES
(1, 'Electricity', '2018-09-24 22:18:42', '2018-09-24 22:18:42', NULL),
(2, 'Water', '2018-09-24 22:19:00', '2018-09-24 22:19:00', NULL),
(3, 'Rent', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bill_histories`
--

CREATE TABLE `bill_histories` (
  `id` int(10) UNSIGNED NOT NULL,
  `firstname` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `lastname` text COLLATE utf8mb4_unicode_ci,
  `bill_type` int(11) DEFAULT NULL,
  `amount` decimal(10,0) DEFAULT NULL,
  `room_id` int(11) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `due_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bill_histories`
--

INSERT INTO `bill_histories` (`id`, `firstname`, `created_at`, `updated_at`, `lastname`, `bill_type`, `amount`, `room_id`, `start_date`, `end_date`, `due_date`) VALUES
(307, 'Mary Jane', '2018-10-26 20:42:44', '2018-10-26 20:42:44', 'San Pedro', 1, '13000', NULL, '2018-10-03', '2018-10-17', '2018-10-25'),
(308, 'Jerome', '2018-10-26 20:43:52', '2018-10-26 20:43:52', 'Ramos', 2, '14000', 9, '2018-10-18', '2018-10-19', '2018-10-25'),
(309, 'Mary Jane', '2018-10-27 21:30:25', '2018-10-27 21:30:25', NULL, NULL, '1000', NULL, NULL, NULL, NULL),
(310, 'Irish Kate', '2018-10-27 21:33:16', '2018-10-27 21:33:16', NULL, NULL, '1000', NULL, NULL, NULL, NULL),
(311, 'Mary Jane', '2018-10-27 21:39:12', '2018-10-27 21:39:12', 'San Pedro', 1, '667', 5, '2018-10-04', '2018-10-05', '2018-10-06'),
(312, 'Irish Kate', '2018-10-27 21:39:12', '2018-10-27 21:39:12', 'Palaming', 1, '667', 5, '2018-10-04', '2018-10-05', '2018-10-06'),
(313, 'Kay Karen', '2018-10-27 21:39:12', '2018-10-27 21:39:12', 'Andarino', 1, '667', 5, '2018-10-04', '2018-10-05', '2018-10-06'),
(314, 'Jason', '2018-10-27 23:37:54', '2018-10-27 23:37:54', NULL, NULL, '4000', NULL, NULL, NULL, NULL),
(315, 'Jason', '2018-10-27 23:43:07', '2018-10-27 23:43:07', NULL, NULL, '5000', NULL, NULL, NULL, NULL),
(316, 'Jason', '2018-10-27 23:51:25', '2018-10-27 23:51:25', 'Siervo', 1, '6', NULL, '2018-10-03', '2018-10-04', '2018-10-05'),
(317, 'Jason', '2018-10-27 23:55:00', '2018-10-27 23:55:00', 'Siervo', 1, '10', NULL, '2018-10-04', '2018-10-05', '2018-10-06'),
(318, 'Irish Kate', '2018-10-27 23:57:49', '2018-10-27 23:57:49', 'Palaming', 2, '8000', NULL, '2018-10-04', '2018-10-05', '2018-10-06'),
(319, 'Jason', '2018-10-27 23:59:50', '2018-10-27 23:59:50', 'Siervo', 1, '90000', NULL, '2018-10-04', '2018-10-05', '2018-10-06'),
(320, 'Jason', '2018-10-28 00:12:51', '2018-10-28 00:12:51', 'Siervo', 1, '3', NULL, '2018-10-23', '2018-10-24', '2018-10-25'),
(321, 'Jason', '2018-10-28 07:28:17', '2018-10-28 07:28:17', 'Siervo', 1, '5', NULL, '2018-10-03', '2018-10-04', '2018-10-05'),
(322, 'Jason', '2018-10-28 07:28:35', '2018-10-28 07:28:35', 'Siervo', 1, '5', NULL, '2018-10-03', '2018-10-04', '2018-10-05'),
(323, 'Jason', '2018-10-28 07:30:51', '2018-10-28 07:30:51', 'Siervo', 1, '5', NULL, '2018-10-03', '2018-10-04', '2018-10-05'),
(324, 'Jason', '2018-10-28 07:31:22', '2018-10-28 07:31:22', 'Siervo', 1, '5', NULL, '2018-10-03', '2018-10-04', '2018-10-05'),
(325, 'Jason', '2018-10-28 07:32:18', '2018-10-28 07:32:18', 'Siervo', 1, '5', NULL, '2018-10-03', '2018-10-04', '2018-10-05'),
(326, 'Jason', '2018-10-28 07:34:41', '2018-10-28 07:34:41', 'Siervo', 1, '5', NULL, '2018-10-03', '2018-10-04', '2018-10-05'),
(327, 'Jason', '2018-10-28 07:50:28', '2018-10-28 07:50:28', 'Siervo', 1, '5', NULL, '2018-10-03', '2018-10-04', '2018-10-05'),
(328, 'Jason', '2018-10-28 07:51:17', '2018-10-28 07:51:17', 'Siervo', 1, '5', NULL, '2018-10-03', '2018-10-04', '2018-10-05'),
(329, 'Jason', '2018-10-28 07:52:04', '2018-10-28 07:52:04', 'Siervo', 1, '5', NULL, '2018-10-03', '2018-10-04', '2018-10-05'),
(330, 'Jason', '2018-10-28 07:59:06', '2018-10-28 07:59:06', 'Siervo', 1, '5', NULL, '2018-10-03', '2018-10-04', '2018-10-05'),
(331, 'Jason', '2018-10-28 08:03:41', '2018-10-28 08:03:41', 'Siervo', 1, '5', NULL, '2018-10-03', '2018-10-04', '2018-10-05'),
(332, 'Jason', '2018-10-28 08:40:37', '2018-10-28 08:40:37', 'Siervo', 1, '5', NULL, '2018-10-03', '2018-10-04', '2018-10-05'),
(333, 'Jason', '2018-10-28 08:42:35', '2018-10-28 08:42:35', 'Siervo', 1, '5', NULL, '2018-10-03', '2018-10-04', '2018-10-05'),
(334, 'Jason', '2018-10-28 09:01:52', '2018-10-28 09:01:52', 'Siervo', 1, '5', NULL, '2018-10-03', '2018-10-04', '2018-10-05'),
(335, 'Jason', '2018-10-28 09:06:14', '2018-10-28 09:06:14', 'Siervo', 1, '5', NULL, '2018-10-04', '2018-10-05', '2018-10-06'),
(336, 'Jason', '2018-10-28 09:07:16', '2018-10-28 09:07:16', 'Siervo', 1, '5', NULL, '2018-10-04', '2018-10-05', '2018-10-06'),
(337, 'Jason', '2018-10-28 10:06:24', '2018-10-28 10:06:24', 'Siervo', 1, '5', NULL, '2018-10-04', '2018-10-05', '2018-10-06'),
(338, 'Jason', '2018-10-28 10:37:02', '2018-10-28 10:37:02', 'Siervo', 1, '5', NULL, '2018-10-04', '2018-10-05', '2018-10-06'),
(339, 'Jason', '2018-10-28 10:39:00', '2018-10-28 10:39:00', 'Siervo', 1, '5', NULL, '2018-10-04', '2018-10-05', '2018-10-06'),
(340, 'Jason', '2018-10-28 10:40:38', '2018-10-28 10:40:38', 'Siervo', 1, '5', NULL, '2018-10-04', '2018-10-05', '2018-10-06'),
(341, 'Jason', '2018-10-28 10:42:40', '2018-10-28 10:42:40', 'Siervo', 1, '5', NULL, '2018-10-04', '2018-10-05', '2018-10-06'),
(342, 'Jason', '2018-10-28 10:43:27', '2018-10-28 10:43:27', 'Siervo', 1, '5', NULL, '2018-10-04', '2018-10-05', '2018-10-06'),
(343, 'Jason', '2018-10-28 10:44:39', '2018-10-28 10:44:39', 'Siervo', 1, '5', NULL, '2018-10-04', '2018-10-05', '2018-10-06'),
(344, 'Jason', '2018-10-28 10:45:10', '2018-10-28 10:45:10', 'Siervo', 1, '5', NULL, '2018-10-04', '2018-10-05', '2018-10-06'),
(345, 'Jason', '2018-10-28 10:46:34', '2018-10-28 10:46:34', 'Siervo', 1, '5', NULL, '2018-10-04', '2018-10-05', '2018-10-06'),
(346, 'Jason', '2018-10-28 10:47:16', '2018-10-28 10:47:16', 'Siervo', 1, '5', NULL, '2018-10-04', '2018-10-05', '2018-10-06'),
(347, 'Jason', '2018-10-28 10:47:37', '2018-10-28 10:47:37', 'Siervo', 1, '5', NULL, '2018-10-04', '2018-10-05', '2018-10-06'),
(348, 'Jason', '2018-10-28 10:49:21', '2018-10-28 10:49:21', 'Siervo', 1, '5', NULL, '2018-10-04', '2018-10-05', '2018-10-06'),
(349, 'Jason', '2018-10-28 10:50:46', '2018-10-28 10:50:46', 'Siervo', 1, '5', NULL, '2018-10-04', '2018-10-05', '2018-10-06'),
(350, 'Jason', '2018-10-28 10:51:48', '2018-10-28 10:51:48', 'Siervo', 1, '5', NULL, '2018-10-04', '2018-10-05', '2018-10-06'),
(351, 'Jason', '2018-10-28 10:54:17', '2018-10-28 10:54:17', 'Siervo', 1, '5', NULL, '2018-10-04', '2018-10-05', '2018-10-06'),
(352, 'Jason', '2018-10-28 10:55:41', '2018-10-28 10:55:41', 'Siervo', 1, '5', NULL, '2018-10-04', '2018-10-05', '2018-10-06'),
(353, 'Jason', '2018-10-28 10:57:37', '2018-10-28 10:57:37', 'Siervo', 1, '1000', NULL, '2018-10-03', '2018-10-04', '2018-10-05'),
(354, 'Jason', '2018-10-28 11:51:06', '2018-10-28 11:51:06', 'Siervo', 1, '5000', NULL, '2018-10-03', '2018-10-04', '2018-10-05'),
(355, 'Mary Jane', '2018-10-28 20:21:50', '2018-10-28 20:21:50', 'San Pedro', 1, '12345', NULL, '2018-10-30', '2018-11-03', '2018-11-01'),
(356, 'Mary Jane', '2018-10-28 20:22:58', '2018-10-28 20:22:58', 'San Pedro', 1, '12345', NULL, '2018-10-30', '2018-11-03', '2018-11-01'),
(357, 'Jason', '2018-10-28 20:28:10', '2018-10-28 20:28:10', 'Siervo', 1, '13000', NULL, '2018-10-29', '2018-10-30', '2018-10-31');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `order`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, 'Category 1', 'category-1', '2018-09-06 00:01:35', '2018-09-06 00:01:35'),
(2, NULL, 1, 'Category 2', 'category-2', '2018-09-06 00:01:35', '2018-09-06 00:01:35');

-- --------------------------------------------------------

--
-- Table structure for table `datavisualizations`
--

CREATE TABLE `datavisualizations` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This :attribute field is required.\"}}}', 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This :attribute field is required.\"}}}', 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, NULL, 11),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 1, 0, 0, 0, NULL, 12),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 6),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":\"0\",\"taggable\":\"0\"}', 8),
(12, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, NULL, 10),
(13, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(14, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This :attribute field is a must.\"}}}', 2),
(15, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(16, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(17, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(18, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"4\",\"id\":\"name_id\"},\"validation\":{\"rule\":\"required|max:20\",\"messages\":{\"required\":\"This :attribute field is required.\",\"max\":\"This :attribute field maximum :max.\"}}}', 2),
(19, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, NULL, 4),
(20, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 1, 0, 0, 0, NULL, 5),
(21, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"4\",\"id\":\"display_name_id\"},\"validation\":{\"rule\":\"required|max:20\",\"messages\":{\"required\":\"This :attribute field is required.\",\"max\":\"This :attribute field maximum :max.\"}}}', 3),
(22, 1, 'role_id', 'text', 'Role', 0, 1, 1, 1, 1, 1, NULL, 7),
(23, 4, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '', 1),
(24, 4, 'parent_id', 'select_dropdown', 'Parent', 0, 0, 1, 1, 1, 1, '{\"default\":\"\",\"null\":\"\",\"options\":{\"\":\"-- None --\"},\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}', 2),
(25, 4, 'order', 'text', 'Order', 1, 1, 1, 1, 1, 1, '{\"default\":1}', 3),
(26, 4, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '', 4),
(27, 4, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"name\"}}', 5),
(28, 4, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, '', 6),
(29, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '', 7),
(30, 5, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '', 1),
(31, 5, 'author_id', 'text', 'Author', 1, 0, 1, 1, 0, 1, '', 2),
(32, 5, 'category_id', 'text', 'Category', 1, 0, 1, 1, 1, 0, '', 3),
(33, 5, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '', 4),
(34, 5, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, '', 5),
(35, 5, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, '', 6),
(36, 5, 'image', 'image', 'Post Image', 0, 1, 1, 1, 1, 1, '{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}', 7),
(37, 5, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:posts,slug\"}}', 8),
(38, 5, 'meta_description', 'text_area', 'Meta Description', 1, 0, 1, 1, 1, 1, '', 9),
(39, 5, 'meta_keywords', 'text_area', 'Meta Keywords', 1, 0, 1, 1, 1, 1, '', 10),
(40, 5, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"DRAFT\",\"options\":{\"PUBLISHED\":\"published\",\"DRAFT\":\"draft\",\"PENDING\":\"pending\"}}', 11),
(41, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, '', 12),
(42, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '', 13),
(43, 5, 'seo_title', 'text', 'SEO Title', 0, 1, 1, 1, 1, 1, '', 14),
(44, 5, 'featured', 'checkbox', 'Featured', 1, 1, 1, 1, 1, 1, '', 15),
(45, 6, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '', 1),
(46, 6, 'author_id', 'text', 'Author', 1, 0, 0, 0, 0, 0, '', 2),
(47, 6, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '', 3),
(48, 6, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, '', 4),
(49, 6, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, '', 5),
(50, 6, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\"},\"validation\":{\"rule\":\"unique:pages,slug\"}}', 6),
(51, 6, 'meta_description', 'text', 'Meta Description', 1, 0, 1, 1, 1, 1, '', 7),
(52, 6, 'meta_keywords', 'text', 'Meta Keywords', 1, 0, 1, 1, 1, 1, '', 8),
(53, 6, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"INACTIVE\",\"options\":{\"INACTIVE\":\"INACTIVE\",\"ACTIVE\":\"ACTIVE\"}}', 9),
(54, 6, 'created_at', 'timestamp', 'Created At', 1, 1, 1, 0, 0, 0, '', 10),
(55, 6, 'updated_at', 'timestamp', 'Updated At', 1, 0, 0, 0, 0, 0, '', 11),
(56, 6, 'image', 'image', 'Page Image', 0, 1, 1, 1, 1, 1, '', 12),
(74, 8, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(75, 8, 'room_id', 'text', 'Room Id', 0, 1, 1, 1, 1, 1, NULL, 2),
(76, 8, 'floor_no', 'number', 'Floor No', 0, 1, 1, 1, 1, 1, NULL, 3),
(77, 8, 'capacity', 'number', 'Capacity', 0, 1, 1, 1, 1, 1, NULL, 4),
(78, 8, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 5),
(79, 8, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 6),
(94, 11, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(95, 11, 'billMgmt_id', 'number', 'BillMgmt Id', 1, 1, 1, 1, 1, 1, NULL, 2),
(96, 11, 'room_id', 'text', 'Room Id', 1, 1, 1, 1, 1, 1, NULL, 3),
(97, 11, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 4),
(98, 11, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 5),
(99, 11, 'bill_mgmt_hasone_room_relationship', 'relationship', 'rooms', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Room\",\"table\":\"rooms\",\"type\":\"hasOne\",\"column\":\"room_id\",\"key\":\"id\",\"label\":\"room_id\",\"pivot_table\":\"bill_mgmt\",\"pivot\":\"0\",\"taggable\":null}', 6),
(106, 14, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(107, 14, 'amount', 'text', 'Amount', 0, 0, 1, 1, 1, 0, NULL, 2),
(108, 14, 'payment_date', 'date', 'Payment Date', 0, 0, 1, 1, 1, 0, NULL, 3),
(109, 14, 'bill_id', 'text', 'Bill Id', 0, 0, 1, 1, 1, 0, NULL, 4),
(110, 14, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 5),
(111, 14, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 6),
(112, 15, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(113, 15, 'amount', 'number', 'Amount', 0, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"3\",\"id\":\"amount_id\"},\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This :attribute field is required.\"}}}', 8),
(116, 15, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 1, NULL, 13),
(117, 15, 'updated_at', 'timestamp', 'Updated At', 0, 0, 1, 0, 0, 0, NULL, 14),
(127, 16, 'floor_no', 'number', 'Floor No', 0, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"3\",\"id\":\"floor_no_id\"},\"validation\":{\"rule\":\"required|integer|max:2\",\"messages\":{\"required\":\"This :attribute field is a must.\",\"integer\":\"This :attribute field should be a number.\",\"max\":\"This :attribute field maximum :max.\"}}}', 3),
(128, 16, 'capacity', 'number', 'Capacity', 0, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"3\",\"id\":\"capacity_id\"},\"validation\":{\"rule\":\"required|integer\",\"messages\":{\"required\":\"This :attribute field is a must.\",\"integer\":\"This :attribute field should be a number.\"}}}', 4),
(129, 16, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 1, NULL, 7),
(130, 16, 'updated_at', 'timestamp', 'Updated At', 0, 0, 1, 0, 0, 0, NULL, 8),
(131, 16, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(132, 16, 'room_no', 'text', 'Room Number', 0, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"3\",\"id\":\"room_no_id\"},\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This :attribute field is a must.\"}}}', 2),
(133, 17, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(134, 17, 'lastname', 'text', 'Last Name', 1, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"3\",\"id\":\"lastname_id\"},\"validation\":{\"rule\":\"required|max:50\",\"messages\":{\"required\":\"This :attribute field is required.\",\"max\":\"This :attribute field maximum :max\"}}}', 3),
(135, 17, 'firstname', 'text', 'First Name', 1, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"3\",\"id\":\"firstname_id\"},\"validation\":{\"rule\":\"required|max:50\",\"messages\":{\"required\":\"This :attribute field is required.\",\"max\":\"This :attribute field maximum :max\"}}}', 4),
(136, 17, 'middlename', 'text', 'Middle Name', 0, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"3\",\"id\":\"middlename_id\"},\"validation\":{\"rule\":\"required|max:50\",\"messages\":{\"required\":\"This :attribute field is required\",\"max\":\"This :attribute field maximum :max\"}}}', 5),
(138, 17, 'birthdate', 'date', 'Date of Birth', 1, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"2\",\"id\":\"birthdate_id\"},\"validation\":{\"rule\":\"required|date\",\"messages\":{\"required\":\"This :attribute field is required.\",\"date\":\"This :attribute field should be a date.\"}}}', 8),
(139, 17, 'email', 'text', 'Email', 1, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"3\",\"id\":\"email_id\"},\"validation\":{\"rule\":\"required|max:60\",\"messages\":{\"required\":\"This :attribute field is required.\",\"max\":\"This :attribute field maximum :max\"}}}', 16),
(145, 17, 'startofcontract', 'date', 'Start of Contract', 1, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"3\",\"id\":\"startOfContract_id\"},\"validation\":{\"rule\":\"required|date\",\"messages\":{\"required\":\"This :attribute field is required.\",\"date\":\"This :attribute field should be a date.\"}}}', 26),
(146, 17, 'endofcontract', 'date', 'End of Contract', 1, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"3\",\"id\":\"endOfContract_id\"},\"validation\":{\"rule\":\"required|date\",\"messages\":{\"required\":\"This :attribute field is required.\",\"date\":\"This :attribute field should be a date.\"}}}', 27),
(147, 17, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 1, '{\"display\":{\"width\":\"6\",\"id\":\"created_at_id\"}}', 57),
(148, 17, 'updated_at', 'timestamp', 'Updated At', 0, 0, 1, 0, 0, 0, '{\"display\":{\"width\":\"6\",\"id\":\"updated_at_id\"}}', 58),
(149, 17, 'username', 'text', 'Username', 1, 0, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This :attribute field is required.\"}}}', 23),
(150, 17, 'password', 'text', 'Password', 1, 0, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This :attribute field is required.\"}}}', 24),
(154, 17, 'room_id', 'text', 'Room Id', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"3\",\"id\":\"room_id\"},\"validation\":{\"rule\":\"required|max:3\",\"messages\":{\"required\":\"This :attribute field is required.\",\"max\":\"This :attribute field maximum :max\"}}}', 32),
(156, 20, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(157, 20, 'billing_id', 'text', 'Billing Id', 0, 1, 1, 1, 1, 1, NULL, 2),
(158, 20, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 3),
(159, 20, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(160, 15, 'billing_belongsto_room_relationship', 'relationship', 'Room', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Room\",\"table\":\"rooms\",\"type\":\"belongsTo\",\"column\":\"room_id\",\"key\":\"id\",\"label\":\"room_no\",\"pivot_table\":\"bill_histories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 10),
(161, 15, 'billing_belongsto_tenant_relationship', 'relationship', 'Tenant', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Tenant\",\"table\":\"tenants\",\"type\":\"belongsTo\",\"column\":\"tenant_id\",\"key\":\"id\",\"label\":\"lastname\",\"pivot_table\":\"bill_histories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 12),
(162, 15, 'tenant_id', 'text', 'Tenant Id', 0, 1, 1, 1, 1, 1, NULL, 11),
(163, 15, 'room_id', 'text', 'Room Id', 0, 1, 1, 1, 1, 1, NULL, 9),
(169, 15, 'bill_type', 'text', 'Bill Type', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This :attribute field is required.\"}}}', 2),
(174, 15, 'start_date', 'date', 'Start Date', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"2\",\"id\":\"start_date_id\"},\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This :attribute field is required.\"}}}', 4),
(175, 15, 'end_date', 'date', 'End Date', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"2\",\"id\":\"end_date_id\"},\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This :attribute field is required.\"}}}', 5),
(176, 15, 'reading', 'number', 'Reading', 0, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"3\",\"id\":\"reading_id\"}}', 7),
(178, 22, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(179, 22, 'firstname', 'text', 'Firstname', 0, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"3\",\"id\":\"firstname_id\"},\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This :attribute field is a must.\"}}}', 2),
(180, 22, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 1, 0, 1, NULL, 12),
(181, 22, 'updated_at', 'timestamp', 'Updated At', 0, 0, 1, 0, 0, 0, NULL, 13),
(183, 22, 'amount', 'number', 'Amount', 0, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"3\",\"id\":\"amount_id\"},\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This :attribute field is a must.\"}}}', 11),
(184, 22, 'lastname', 'text', 'Lastname', 0, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"3\",\"id\":\"lastname_id\"},\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This :attribute field is a must.\"}}}', 3),
(185, 22, 'bill_type', 'number', 'Bill Type', 0, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"3\",\"id\":\"bill_type_id\"},\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This :attribute field is a must.\"}}}', 9),
(187, 24, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(188, 24, 'bill_type', 'text', 'Bill Type', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This :attribute field is required.\"}},\"display\":{\"width\":\"4\",\"id\":\"bill_type_id\"}}', 2),
(189, 24, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 1, NULL, 4),
(190, 24, 'updated_at', 'timestamp', 'Updated At', 0, 0, 1, 0, 0, 0, NULL, 5),
(191, 15, 'billing_belongsto_billing_category_relationship', 'relationship', 'Bill Type', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\BillingCategory\",\"table\":\"billing_categories\",\"type\":\"belongsTo\",\"column\":\"bill_type\",\"key\":\"id\",\"label\":\"bill_type\",\"pivot_table\":\"bill_histories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 3),
(192, 25, 'id', 'hidden', 'Id', 1, 1, 1, 1, 1, 0, '{\"display\":{\"width\":\"3\",\"id\":\"id\"}}', 1),
(195, 25, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This :attribute field is required.\"}}}', 6),
(196, 25, 'details', 'text', 'Details', 0, 0, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This :attribute field is required.\"}}}', 7),
(197, 25, 'severity', 'select_dropdown', 'Severity', 0, 1, 1, 1, 1, 1, '{\"options\":{\"1\":\"1\",\"2\":\"2\",\"3\":\"3\",\"4\":\"4\",\"5\":\"5\",\"\":\"---------\"},\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This :attribute field is required.\"}}}', 8),
(198, 25, 'solved_status', 'text', 'Current Status', 0, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"3\",\"id\":\"solved_status\"}}', 9),
(199, 25, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 1, NULL, 10),
(200, 25, 'updated_at', 'timestamp', 'Updated At', 0, 0, 1, 0, 0, 0, NULL, 11),
(201, 25, 'problem_belongsto_user_relationship', 'relationship', 'users', 0, 1, 1, 1, 1, 1, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"user_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"bill_histories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 3),
(202, 25, 'problem_belongsto_tenant_relationship', 'relationship', 'tenants', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Tenant\",\"table\":\"tenants\",\"type\":\"belongsTo\",\"column\":\"complainant_id\",\"key\":\"id\",\"label\":\"lastname\",\"pivot_table\":\"bill_histories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 5),
(203, 25, 'user_id', 'number', 'User Id', 0, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"3\",\"id\":\"user_id\"}}', 2),
(204, 25, 'complainant_id', 'number', 'Complainant Id', 0, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"3\",\"id\":\"complainant_id\"}}', 4),
(205, 17, 'cellNo', 'text', 'Cellphone Number', 1, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"3\",\"id\":\"cellNo_id\"},\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This :attribute field is required.\"}}}', 14),
(206, 17, 'homeAddress', 'text', 'Home Address', 1, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"6\",\"id\":\"homeAddress_id\"},\"validation\":{\"rule\":\"required|max:100\",\"messages\":{\"required\":\"This :attribute field is required.\",\"max\":\"This :attribute field maximum :max\"}}}', 17),
(207, 17, 'nickname', 'text', 'Nick Name', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"3\",\"id\":\"nickname_id\"},\"validation\":{\"rule\":\"max:40\",\"messages\":{\"max\":\"This :attribute field maximum :max\"}}}', 6),
(208, 17, 'nationality', 'text', 'Nationality', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"3\",\"id\":\"nationality_id\"},\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This :attribute field is required.\"}}}', 7),
(209, 17, 'age', 'number', 'Age', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"1\",\"id\":\"age_id\"},\"validation\":{\"rule\":\"required|integer|max:2\",\"messages\":{\"required\":\"This :attribute field is required.\",\"integer\":\"This :attribute field only accept integers.\",\"max\":\"This :attribute field maximum :max\"}}}', 9),
(210, 17, 'birthPlace', 'text', 'Place of Birth', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"3\",\"id\":\"birthPlace_id\"},\"validation\":{\"rule\":\"required|max:35\",\"messages\":{\"required\":\"This :attribute field is required.\",\"max\":\"This :attribute field maximum :max\"}}}', 10),
(211, 17, 'religion', 'text', 'Religion', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"3\",\"id\":\"religion_id\"},\"validation\":{\"rule\":\"required|max:35\",\"messages\":{\"required\":\"This :attribute field is required.\",\"max\":\"This :attribute field maximum :max\"}}}', 11),
(212, 17, 'sex', 'select_dropdown', 'Sex', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"1\",\"id\":\"sex_id\"},\"default\":\"M\",\"options\":{\"M\":\"Male\",\"F\":\"Female\"},\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This :attribute field is required.\"}}}', 12),
(213, 17, 'civilStatus', 'select_dropdown', 'Civil Status', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"1\",\"id\":\"civilStatus_id\"},\"default\":\"Single\",\"options\":{\"Single\":\"Single\",\"Married\":\"Married\",\"Divorced\":\"Divorced\",\"Widowed\":\"Widowed\"},\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This :attribute field is required.\"}}}', 13),
(214, 17, 'landline', 'text', 'Landline Number', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"3\",\"id\":\"landline_id\"},\"validation\":{\"rule\":\"integer\",\"messages\":{\"integer\":\"This :attribute field should contain numbers only.\"}}}', 15),
(215, 17, 'mailingAddress', 'text', 'Mailing Address', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"6\",\"id\":\"mailingAddress_id\"},\"validation\":{\"rule\":\"required|max:100\",\"messages\":{\"required\":\"This :attribute field is a must.\",\"max\":\"This :attribute field maximum :max\"}}}', 18),
(216, 17, 'secondarySchool', 'text', 'Secondary School', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"3\",\"id\":\"secondarySchool_id\"},\"validation\":{\"rule\":\"max:70\",\"messages\":{\"max\":\"This :attribute field maximum :max\"}}}', 19),
(217, 17, 'college', 'text', 'College/University', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"3\",\"id\":\"college_id\"},\"validation\":{\"rule\":\"max:70\",\"messages\":{\"max\":\"This :attribute field maximum :max\"}}}', 20),
(218, 17, 'course', 'text', 'Course', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"3\",\"id\":\"course_id\"},\"validation\":{\"rule\":\"max:60\",\"messages\":{\"max\":\"This :attribute field maximum :max\"}}}', 21),
(219, 17, 'collegeYear', 'number', 'College Year', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"3\",\"id\":\"collegeYear_id\"},\"validation\":{\"rule\":\"max:10\",\"messages\":{\"max\":\"This :attribute field maximum :max\"}}}', 22),
(220, 17, 'fatherName', 'text', 'Father\'s Name', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"6\",\"id\":\"fatherName_id\"},\"validation\":{\"rule\":\"max:90\",\"messages\":{\"max\":\"This :attribute field maximum :max\"}}}', 33),
(221, 17, 'fatherOccupation', 'text', 'Father\'s Occupation', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"3\",\"id\":\"fatherOccupation_id\"},\"validation\":{\"rule\":\"max:40\",\"messages\":{\"max\":\"This :attribute field maximum :max\"}}}', 34),
(222, 17, 'fatherOffice', 'text', 'Office Address', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"6\",\"id\":\"fatherOffice_id\"},\"validation\":{\"rule\":\"max:95\",\"messages\":{\"max\":\"This :attribute field maximum :max\"}}}', 36),
(223, 17, 'fatherOfficeTel', 'text', 'Office/Business Landline No.', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"3\",\"id\":\"fatherOfficeTel_id\"},\"validation\":{\"rule\":\"max:20\",\"messages\":{\"max\":\"This :attribute field maximum :max\"}}}', 35),
(224, 17, 'fatherOfficeCel', 'text', 'Cellphone Number', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"3\",\"id\":\"fatherOfficeCel_id\"},\"validation\":{\"rule\":\"max:15\",\"messages\":{\"max\":\"This :attribute field maximum :max\"}}}', 37),
(225, 17, 'fatherEmail', 'text', 'Email', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"3\",\"id\":\"fatherEmail_id\"},\"validation\":{\"rule\":\"max:65\",\"messages\":{\"max\":\"This :attribute field maximum :max\"}}}', 38),
(226, 17, 'motherName', 'text', 'Mother\'s Name', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"6\",\"id\":\"motherName_id\"},\"validation\":{\"rule\":\"max:90\",\"messages\":{\"max\":\"This :attribute field maximum :max\"}}}', 39),
(227, 17, 'motherOccupation', 'text', 'Mother\'s Occupation', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"3\",\"id\":\"motherOccupation_id\"},\"validation\":{\"rule\":\"max:40\",\"messages\":{\"max\":\"This :attribute field maximum :max\"}}}', 40),
(228, 17, 'motherOffice', 'text', 'Office Address', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"6\",\"id\":\"motherOffice_id\"},\"validation\":{\"rule\":\"max:100\",\"messages\":{\"max\":\"This :attribute field maximum :max\"}}}', 42),
(229, 17, 'motherOfficeTel', 'text', 'Office/Business Landline No', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"3\",\"id\":\"motherOfficetel_id\"},\"validation\":{\"rule\":\"max:20\",\"messages\":{\"max\":\"This :attribute field maximum :max\"}}}', 41),
(230, 17, 'motherOfficeCel', 'text', 'Cellphone Number', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"3\",\"id\":\"motherOfficeCel_id\"},\"validation\":{\"rule\":\"max:15\",\"messages\":{\"max\":\"This :attribute field maximum :max\"}}}', 43),
(231, 17, 'motherEmail', 'text', 'Email', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"3\",\"id\":\"motherEmail_id\"},\"validation\":{\"rule\":\"max:65\",\"messages\":{\"max\":\"This :attribute field maximum :max\"}}}', 44),
(232, 17, 'guardianName', 'text', 'Guardian\'s Name', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"6\",\"id\":\"GuardianName_id\"},\"validation\":{\"rule\":\"required|max:90\",\"messages\":{\"max\":\"This :attribute field maximum :max\"}}}', 45),
(233, 17, 'guardianRelation', 'text', 'Relationship', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"3\",\"id\":\"GuardianRelation_id\"},\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This :attribute field is a must.\"}}}', 46),
(234, 17, 'guardianAge', 'number', 'Age', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"3\",\"id\":\"GuardianAge_id\"},\"validation\":{\"rule\":\"numeric|max:2\",\"messages\":{\"numeric\":\"This :attribute field should contain integers and decimals only.\"}}}', 47),
(235, 17, 'guardianAddress', 'text', 'Address', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"6\",\"id\":\"GuardianAddress_id\"},\"validation\":{\"rule\":\"required|max:100\",\"messages\":{\"required\":\"This :attribute field is required.\",\"max\":\"This :attribute field maximum :max\"}}}', 48),
(236, 17, 'guardianEmail', 'text', 'Email', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"3\",\"id\":\"GuardianEmail_id\"},\"validation\":{\"rule\":\"required|max:65\",\"messages\":{\"required\":\"This :attribute field is required.\",\"max\":\"This :attribute field maximum :max\"}}}', 49),
(238, 17, 'guardianTel', 'text', 'Landline Number', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"3\",\"id\":\"GuardianTel_id\"},\"validation\":{\"rule\":\"max:20\",\"messages\":{\"max\":\"This :attribute field maximum :max\"}}}', 51),
(239, 17, 'guardianOccupation', 'text', 'GuardianOccupation', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"3\",\"id\":\"GuardianOccupation_id\"},\"validation\":{\"rule\":\"max:30\",\"messages\":{\"max\":\"This :attribute field maximum :max\"}}}', 52),
(240, 17, 'emergencyContName', 'text', 'Emergency Contact Name', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"6\",\"id\":\"emergencyContName_id\"},\"validation\":{\"rule\":\"required|max:90\",\"messages\":{\"required\":\"This :attribute field is required.\",\"max\":\"This :attribute field maximum :max\"}}}', 53),
(241, 17, 'emergencyContNo', 'text', 'Contact Number', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"6\",\"id\":\"emergencyContNo_id\"},\"validation\":{\"rule\":\"required|max:20\",\"messages\":{\"required\":\"This :attribute field is required.\",\"max\":\"This :attribute field maximum :max\"}}}', 54),
(242, 17, 'signatoryName', 'text', 'Signatory Name', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"6\",\"id\":\"signatoryName_id\"},\"validation\":{\"rule\":\"required|max:90\",\"messages\":{\"required\":\"This :attribute field is required.\",\"max\":\"This :attribute field maximum :max\"}}}', 55),
(243, 17, 'signatoryAddress', 'text', 'Signatory Address', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"6\",\"id\":\"signatoryAddress_id\"},\"validation\":{\"rule\":\"required|max:100\",\"messages\":{\"required\":\"This :attribute field is required.\",\"max\":\"This :attribute field maximum :max\"}}}', 56),
(244, 17, 'contractLength', 'text', 'Length of Stay', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"3\",\"id\":\"contractLength_id\"},\"validation\":{\"rule\":\"required|max:10\",\"messages\":{\"required\":\"This :attribute field is required.\",\"max\":\"This :attribute field maximum :max\"}}}', 25),
(246, 17, 'guardianCel', 'text', 'Cellphone Number', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"3\",\"id\":\"GuardianCel_id\"},\"validation\":{\"rule\":\"required|max:15\",\"messages\":{\"required\":\"This :attribute field is required.\",\"max\":\"This :attribute field maximum :max\"}}}', 50),
(247, 17, 'tenant_belongsto_room_relationship', 'relationship', 'rooms', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Room\",\"table\":\"rooms\",\"type\":\"belongsTo\",\"column\":\"room_id\",\"key\":\"id\",\"label\":\"room_no\",\"pivot_table\":\"bill_histories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 31),
(248, 26, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(249, 26, 'from', 'text', 'From', 1, 1, 1, 1, 1, 1, NULL, 2),
(250, 26, 'to', 'text', 'To', 1, 1, 1, 1, 1, 1, NULL, 3),
(251, 26, 'message', 'text', 'Message', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This :attribute field is a must.\"}}}', 4),
(252, 26, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 5),
(253, 26, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 6),
(254, 27, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(255, 27, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 2),
(256, 27, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 3),
(257, 27, 'event_name', 'text', 'Event Name', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This field is required. Please enter event name.\"}}}', 4),
(258, 27, 'start_date', 'text', 'Start Date', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This field is required. Please enter start date.\"}}}', 5),
(259, 27, 'end_date', 'text', 'End Date', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This field is required. Please enter end date\"}}}', 6),
(260, 27, 'event_price', 'number', 'Event Price', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This field is required. Please enter price.\"}}}', 7),
(261, 30, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(262, 30, 'classification', 'text', 'Classification', 0, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"4\",\"id\":\"tenant_classification_id\"},\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This :attribute field is required.\"}}}', 2),
(263, 30, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 1, NULL, 4),
(264, 30, 'updated_at', 'timestamp', 'Updated At', 0, 0, 1, 0, 0, 1, NULL, 5),
(265, 22, 'room_id', 'text', 'Room Id', 0, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"3\",\"id\":\"room_id_id\"}}', 4),
(266, 17, 'tenant_belongsto_tenant_classification_relationship', 'relationship', 'Tenant Classification', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\TenantClassification\",\"table\":\"tenant_classifications\",\"type\":\"belongsTo\",\"column\":\"classification\",\"key\":\"id\",\"label\":\"classification\",\"pivot_table\":\"bill_histories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 30),
(267, 17, 'classification', 'number', 'Classification', 0, 0, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|max:20\",\"messages\":{\"required\":\"This :attribute field is required.\",\"max\":\"This :attribute field maximum :max\"}}}', 29),
(268, 31, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(269, 31, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"6\",\"id\":\"name_id\"}}', 2),
(270, 31, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"6\",\"id\":\"email_id\"}}', 3),
(271, 31, 'message', 'text', 'Message', 1, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"12\",\"id\":\"message_id\"}}', 4),
(272, 31, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 1, NULL, 5),
(273, 31, 'updated_at', 'timestamp', 'Updated At', 0, 0, 1, 0, 0, 0, NULL, 6),
(274, 16, 'status', 'text', 'Status', 0, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"3\",\"id\":\"capacity_id\"},\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This :attribute field is required\"}}}', 6),
(275, 24, 'short_description', 'text', 'Short Description', 0, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"4\",\"id\":\"short_description_id\"}}', 3),
(276, 30, 'short_description', 'text', 'Short Description', 0, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"4\",\"id\":\"short_description_id\"}}', 3),
(277, 32, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(278, 32, 'tenant_id', 'text', 'Tenant Id', 0, 0, 1, 0, 0, 1, NULL, 2),
(279, 32, 'lastname', 'text', 'Lastname', 0, 1, 1, 0, 0, 1, NULL, 3),
(280, 32, 'firstname', 'text', 'Firstname', 0, 1, 1, 0, 0, 1, NULL, 4),
(281, 32, 'amount', 'text', 'Amount', 0, 1, 1, 0, 0, 1, NULL, 5),
(282, 32, 'payment_method', 'text', 'Payment Method', 0, 1, 1, 0, 0, 1, NULL, 6),
(283, 32, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 1, NULL, 7),
(284, 32, 'updated_at', 'timestamp', 'Updated At', 0, 0, 1, 0, 0, 0, NULL, 8),
(285, 17, 'picture', 'image', 'Picture', 0, 1, 1, 1, 1, 1, '{}', 2),
(286, 16, 'free_slots', 'text', 'Free Slots', 0, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"3\",\"id\":\"free_slots_id\"},\"validation\":{\"rule\":\"required|integer\",\"messages\":{\"required\":\"This :attribute field is a must.\",\"integer\":\"This :attribute field should be a number.\"}}}', 5),
(287, 17, 'monthly_rent', 'text', 'Monthly Rent', 0, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"3\",\"id\":\"monthly_rent_id\"},\"validation\":{\"rule\":\"required|numeric\",\"messages\":{\"required\":\"This :attribute field is required.\",\"numeric\":\"This :attribute field should contain integers and decimals only.\"}}}', 28),
(288, 15, 'due_date', 'date', 'Due Date', 0, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"2\",\"id\":\"due_date_id\"},\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This :attribute field is required.\"}}}', 6),
(289, 22, 'start_date', 'text', 'Start Date', 0, 1, 1, 1, 1, 1, NULL, 6),
(290, 22, 'end_date', 'text', 'End Date', 0, 1, 1, 1, 1, 1, NULL, 7),
(291, 22, 'due_date', 'text', 'Due Date', 0, 1, 1, 1, 1, 1, NULL, 8),
(292, 22, 'bill_history_belongsto_billing_category_relationship', 'relationship', 'Bill Type', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\BillingCategory\",\"table\":\"billing_categories\",\"type\":\"belongsTo\",\"column\":\"bill_type\",\"key\":\"id\",\"label\":\"bill_type\",\"pivot_table\":\"bill_histories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 10),
(293, 22, 'bill_history_belongsto_room_relationship', 'relationship', 'Room No', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Room\",\"table\":\"rooms\",\"type\":\"belongsTo\",\"column\":\"room_id\",\"key\":\"id\",\"label\":\"room_no\",\"pivot_table\":\"bill_histories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 5),
(294, 33, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(295, 33, 'name', 'text', 'Name', 0, 1, 1, 1, 1, 1, NULL, 2),
(296, 33, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 3),
(297, 33, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(298, 34, 'id', 'hidden', 'Id', 1, 1, 1, 1, 1, 1, NULL, 1),
(299, 34, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This :attribute field is required.\"}}}', 2),
(300, 34, 'user_id', 'text', 'Posted by', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This :attribute field is required.\"}}}', 3),
(301, 34, 'message', 'rich_text_box', 'Message', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This :attribute field is required.\"}}}', 5),
(302, 34, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 1, NULL, 6),
(303, 34, 'updated_at', 'timestamp', 'Updated At', 0, 0, 1, 0, 0, 0, NULL, 7),
(304, 34, 'announcement_belongsto_user_relationship', 'relationship', 'Posted by', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"user_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"announcements\",\"pivot\":\"0\",\"taggable\":\"0\"}', 4);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-09-06 00:00:50', '2018-10-18 09:40:04'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-09-06 00:00:53', '2018-10-18 09:28:14'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-09-06 00:00:57', '2018-10-19 10:04:22'),
(4, 'categories', 'categories', 'Category', 'Categories', 'voyager-categories', 'TCG\\Voyager\\Models\\Category', NULL, '', '', 1, 0, NULL, '2018-09-06 00:01:33', '2018-09-06 00:01:33'),
(5, 'posts', 'posts', 'Post', 'Posts', 'voyager-news', 'TCG\\Voyager\\Models\\Post', 'TCG\\Voyager\\Policies\\PostPolicy', '', '', 1, 0, NULL, '2018-09-06 00:01:36', '2018-09-06 00:01:36'),
(6, 'pages', 'pages', 'Page', 'Pages', 'voyager-file-text', 'TCG\\Voyager\\Models\\Page', NULL, '', '', 1, 0, NULL, '2018-09-06 00:01:57', '2018-09-06 00:01:57'),
(8, 'room', 'bill-management', 'Bill Management', 'Bill Managements', 'voyager-home', 'App\\Room', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null}', '2018-09-08 05:43:37', '2018-09-08 05:47:23'),
(11, 'bill_mgmt', 'bill-mgmt', 'Bill Management', 'Bill Managements', 'voyager-dollar', 'App\\Bill_mgmt', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null}', '2018-09-08 07:41:13', '2018-09-08 07:41:13'),
(14, 'billing', 'billing', 'Billing', 'Billings', NULL, 'App\\Billing', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-09-16 22:20:13', '2018-09-16 22:20:13'),
(15, 'billings', 'billings', 'Billing', 'Billings', NULL, 'App\\Billing', NULL, '\\App\\Http\\Controllers\\Voyager\\BillsController', NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null}', '2018-09-16 22:32:59', '2018-09-18 22:23:21'),
(16, 'rooms', 'rooms', 'Room', 'Rooms', 'voyager-home', 'App\\Room', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-09-17 05:57:53', '2018-09-17 05:57:53'),
(17, 'tenants', 'tenants', 'Tenant', 'Tenants', 'voyager-person', 'App\\Tenant', NULL, '\\App\\Http\\Controllers\\Voyager\\TenantsController', NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null}', '2018-09-17 06:50:36', '2018-10-20 12:49:38'),
(20, 'bill_history', 'bill-history', 'Bill History', 'Bill Histories', 'voyager-book', 'App\\BillHistory', NULL, '\\App\\Http\\Controllers\\Voyager\\BillHistoryController', NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null}', '2018-09-18 05:59:01', '2018-09-18 05:59:01'),
(22, 'bill_histories', 'bill-histories', 'Bill History', 'Bill Histories', 'voyager-book', 'App\\BillHistory', NULL, '\\App\\Http\\Controllers\\Voyager\\BillHistoryController', NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null}', '2018-09-19 08:20:24', '2018-09-19 08:20:24'),
(24, 'billing_categories', 'billing-categories', 'Billing Category', 'Billing Categories', 'voyager-categories', 'App\\BillingCategory', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null}', '2018-09-24 22:16:06', '2018-10-01 07:55:48'),
(25, 'problems', 'problems', 'Problem', 'Problems', 'voyager-sound', 'App\\Problem', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null}', '2018-10-01 07:54:57', '2018-10-01 07:54:57'),
(26, 'messages', 'messages', 'Message', 'Messages', NULL, 'App\\Message', NULL, 'MessageController', NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null}', '2018-10-08 19:33:35', '2018-10-08 19:33:35'),
(27, 'events', 'events', 'Event', 'Events', NULL, 'App\\Event', NULL, 'EventsController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-10-08 19:34:24', '2018-10-08 19:34:24'),
(29, 'tenant_classification', 'tenant-classification', 'Tenant Classification', 'Tenant Classifications', 'voyager-group', 'App\\TenantClassification', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null}', '2018-10-15 20:57:52', '2018-10-15 20:57:52'),
(30, 'tenant_classifications', 'tenant-classifications', 'Tenant Classification', 'Tenant Classifications', 'voyager-group', 'App\\TenantClassification', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null}', '2018-10-15 21:12:46', '2018-10-15 21:12:46'),
(31, 'gen_inquiries', 'gen-inquiries', 'General Inquiry', 'General Inquiries', 'voyager-paper-plane', 'App\\GenInquiries', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null}', '2018-10-19 07:24:00', '2018-10-19 07:24:00'),
(32, 'payments', 'payments', 'Payment', 'Payments', NULL, 'App\\Payment', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null}', '2018-10-19 08:43:55', '2018-10-19 08:43:55'),
(33, 'datavisualizations', 'datavisualizations', 'Datavisualization', 'Datavisualizations', NULL, 'App\\DataVisualization', NULL, 'DataVisualizationController', NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null}', '2018-10-28 08:30:35', '2018-10-28 08:31:39'),
(34, 'announcements', 'announcements', 'Announcement', 'Announcements', NULL, 'App\\Announcement', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null}', '2018-11-02 23:17:59', '2018-11-02 23:17:59');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `event_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `event_price` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `created_at`, `updated_at`, `event_name`, `start_date`, `end_date`, `event_price`) VALUES
(1, '2018-10-08 19:36:20', '2018-10-08 19:36:20', 'Sample', '2018-10-09', '2018-10-10', 100),
(2, '2018-10-16 00:48:46', '2018-10-16 00:48:46', 'Party', '2018-10-17', '2018-10-17', 1000),
(3, '2018-10-27 19:51:19', '2018-10-27 19:51:19', 'thesis', '2018-10-03', '2018-10-04', 1000),
(4, '2018-10-28 11:59:16', '2018-10-28 11:59:16', 'Mama\'s Birthday', '2018-10-21', '2018-10-21', 10000),
(5, '2018-10-28 12:02:31', '2018-10-28 12:02:31', 'National Bookstore Sale', '2018-10-21', '2018-10-24', 0);

-- --------------------------------------------------------

--
-- Table structure for table `gen_inquiries`
--

CREATE TABLE `gen_inquiries` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gen_inquiries`
--

INSERT INTO `gen_inquiries` (`id`, `name`, `email`, `message`, `created_at`, `updated_at`) VALUES
(1, 'Jason Siervo', 'jason.siervo@gmail.com', 'asasdasda', '2018-10-08 21:09:59', '2018-10-08 21:09:59');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2018-09-06 00:01:02', '2018-09-06 00:01:02'),
(2, 'Cashier', '2018-09-08 06:48:27', '2018-09-08 06:48:27');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2018-09-06 00:01:02', '2018-09-06 00:01:02', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 13, '2018-09-06 00:01:02', '2018-11-02 23:22:17', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 12, '2018-09-06 00:01:02', '2018-11-02 23:22:17', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 11, '2018-09-06 00:01:02', '2018-11-02 23:22:17', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 17, '2018-09-06 00:01:02', '2018-11-02 23:22:17', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 1, '2018-09-06 00:01:02', '2018-10-01 07:57:01', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 2, '2018-09-06 00:01:02', '2018-10-01 07:57:01', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 3, '2018-09-06 00:01:02', '2018-10-01 07:57:01', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 4, '2018-09-06 00:01:02', '2018-10-01 07:57:01', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 18, '2018-09-06 00:01:02', '2018-11-02 23:22:17', 'voyager.settings.index', NULL),
(11, 1, 'Categories', '', '_self', 'voyager-categories', NULL, NULL, 15, '2018-09-06 00:01:34', '2018-11-02 23:22:17', 'voyager.categories.index', NULL),
(12, 1, 'Data Visualization', '', '_self', 'voyager-news', '#000000', NULL, 14, '2018-09-06 00:01:40', '2018-11-02 23:22:17', 'voyager.posts.index', 'null'),
(13, 1, 'Pages', '', '_self', 'voyager-file-text', NULL, NULL, 16, '2018-09-06 00:02:04', '2018-11-02 23:22:17', 'voyager.pages.index', NULL),
(14, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 5, '2018-09-06 00:02:23', '2018-10-01 07:57:01', 'voyager.hooks', NULL),
(16, 1, 'Tenants', '/admin/tenants', '_self', 'voyager-people', '#000000', 39, 2, '2018-09-06 23:09:30', '2018-10-15 21:04:25', NULL, ''),
(19, 1, 'Rooms', '', '_self', 'voyager-home', NULL, NULL, 4, '2018-09-08 06:41:13', '2018-11-02 23:22:16', 'voyager.rooms.index', NULL),
(24, 1, 'Billings', '', '_self', 'voyager-news', '#000000', 29, 2, '2018-09-16 22:32:59', '2018-10-01 07:56:49', 'voyager.billings.index', 'null'),
(28, 1, 'Tenant\'s Payment History', '', '_self', 'voyager-book', '#000000', 30, 1, '2018-09-18 19:35:22', '2018-09-18 23:32:24', NULL, ''),
(29, 1, 'Bill Management', '', '_self', 'voyager-documentation', '#000000', NULL, 5, '2018-09-18 19:39:23', '2018-11-02 23:22:16', NULL, ''),
(31, 1, 'Bill Histories', '', '_self', 'voyager-book', NULL, 29, 3, '2018-09-19 08:20:25', '2018-10-01 07:56:49', 'voyager.bill-histories.index', NULL),
(32, 1, 'Billing Categories', '', '_self', 'voyager-categories', '#000000', 29, 1, '2018-09-24 22:16:06', '2018-10-01 07:56:49', 'voyager.billing-categories.index', 'null'),
(33, 1, 'Problems', '', '_self', 'voyager-warning', '#000000', NULL, 6, '2018-10-01 07:54:58', '2018-11-02 23:22:17', 'voyager.problems.index', 'null'),
(36, 1, 'Messages', '', '_self', 'voyager-bubble', '#000000', NULL, 7, '2018-10-08 19:33:36', '2018-11-02 23:22:17', 'voyager.messages.index', 'null'),
(37, 1, 'Events', '', '_self', 'voyager-star', '#000000', NULL, 10, '2018-10-08 19:34:25', '2018-11-02 23:22:17', 'voyager.events.index', 'null'),
(39, 1, 'Tenant Management', '', '_self', 'voyager-people', '#000000', NULL, 3, '2018-10-15 21:00:56', '2018-11-02 23:22:16', NULL, ''),
(40, 1, 'Tenant Classifications', '', '_self', 'voyager-group', NULL, 39, 1, '2018-10-15 21:12:46', '2018-10-15 21:14:03', 'voyager.tenant-classifications.index', NULL),
(41, 1, 'General Inquiries', '', '_self', 'voyager-paper-plane', NULL, NULL, 9, '2018-10-19 07:24:01', '2018-11-02 23:22:17', 'voyager.gen-inquiries.index', NULL),
(42, 1, 'Payments', '', '_self', 'voyager-credit-cards', '#000000', 29, 4, '2018-10-19 08:43:56', '2018-10-19 08:45:28', 'voyager.payments.index', 'null'),
(43, 1, 'Datavisualizations', '', '_self', 'voyager-bar-chart', '#000000', NULL, 2, '2018-10-28 08:30:35', '2018-11-02 23:22:16', 'voyager.datavisualizations.index', 'null'),
(44, 1, 'Announcements', '', '_self', 'voyager-sound', '#000000', NULL, 8, '2018-11-02 23:18:00', '2018-11-02 23:22:17', 'voyager.announcements.index', 'null');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `from` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `to` int(11) NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `from`, `to`, `message`, `created_at`, `updated_at`) VALUES
(1, 'admin@admin.com', 2, 'efefrf', '2018-10-26 22:18:31', '2018-10-26 22:18:31');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_05_19_173453_create_menu_table', 1),
(6, '2016_10_21_190000_create_roles_table', 1),
(7, '2016_10_21_190000_create_settings_table', 1),
(8, '2016_11_30_135954_create_permission_table', 1),
(9, '2016_11_30_141208_create_permission_role_table', 1),
(10, '2016_12_26_201236_data_types__add__server_side', 1),
(11, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(12, '2017_01_14_005015_create_translations_table', 1),
(13, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(14, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(15, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(16, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(17, '2017_08_05_000000_add_group_to_settings_table', 1),
(18, '2017_11_26_013050_add_user_role_relationship', 1),
(19, '2017_11_26_015000_create_user_roles_table', 1),
(20, '2018_03_11_000000_add_user_settings', 1),
(21, '2018_03_14_000000_add_details_to_data_types_table', 1),
(22, '2018_03_16_000000_make_settings_value_nullable', 1),
(23, '2016_01_01_000000_create_pages_table', 2),
(24, '2016_01_01_000000_create_posts_table', 2),
(25, '2016_02_15_204651_create_categories_table', 2),
(26, '2017_04_11_000000_alter_post_nullable_fields_table', 2),
(27, '2018_09_15_081646_create_bills_table', 3),
(28, '2018_10_08_072718_billing_billing_category', 4),
(29, '2018_10_09_023438_create_messages_table', 4),
(30, '2018_10_09_023804_create_events_table', 4),
(31, '2018_10_09_045613_create_gen_inquiries_table', 5);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `author_id`, `title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 'Hello World', 'Hang the jib grog grog blossom grapple dance the hempen jig gangway pressgang bilge rat to go on account lugger. Nelsons folly gabion line draught scallywag fire ship gaff fluke fathom case shot. Sea Legs bilge rat sloop matey gabion long clothes run a shot across the bow Gold Road cog league.', '<p>Hello World. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', 'pages/page1.jpg', 'hello-world', 'Yar Meta Description', 'Keyword1, Keyword2', 'ACTIVE', '2018-09-06 00:02:10', '2018-09-06 00:02:10');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(10) UNSIGNED NOT NULL,
  `tenant_id` int(11) DEFAULT NULL,
  `lastname` text COLLATE utf8mb4_unicode_ci,
  `firstname` text COLLATE utf8mb4_unicode_ci,
  `amount` decimal(10,0) DEFAULT NULL,
  `payment_method` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2018-09-06 00:01:03', '2018-09-06 00:01:03'),
(2, 'browse_bread', NULL, '2018-09-06 00:01:03', '2018-09-06 00:01:03'),
(3, 'browse_database', NULL, '2018-09-06 00:01:03', '2018-09-06 00:01:03'),
(4, 'browse_media', NULL, '2018-09-06 00:01:03', '2018-09-06 00:01:03'),
(5, 'browse_compass', NULL, '2018-09-06 00:01:03', '2018-09-06 00:01:03'),
(6, 'browse_menus', 'menus', '2018-09-06 00:01:03', '2018-09-06 00:01:03'),
(7, 'read_menus', 'menus', '2018-09-06 00:01:03', '2018-09-06 00:01:03'),
(8, 'edit_menus', 'menus', '2018-09-06 00:01:03', '2018-09-06 00:01:03'),
(9, 'add_menus', 'menus', '2018-09-06 00:01:04', '2018-09-06 00:01:04'),
(10, 'delete_menus', 'menus', '2018-09-06 00:01:04', '2018-09-06 00:01:04'),
(11, 'browse_roles', 'roles', '2018-09-06 00:01:04', '2018-09-06 00:01:04'),
(12, 'read_roles', 'roles', '2018-09-06 00:01:04', '2018-09-06 00:01:04'),
(13, 'edit_roles', 'roles', '2018-09-06 00:01:04', '2018-09-06 00:01:04'),
(14, 'add_roles', 'roles', '2018-09-06 00:01:04', '2018-09-06 00:01:04'),
(15, 'delete_roles', 'roles', '2018-09-06 00:01:04', '2018-09-06 00:01:04'),
(16, 'browse_users', 'users', '2018-09-06 00:01:04', '2018-09-06 00:01:04'),
(17, 'read_users', 'users', '2018-09-06 00:01:04', '2018-09-06 00:01:04'),
(18, 'edit_users', 'users', '2018-09-06 00:01:04', '2018-09-06 00:01:04'),
(19, 'add_users', 'users', '2018-09-06 00:01:04', '2018-09-06 00:01:04'),
(20, 'delete_users', 'users', '2018-09-06 00:01:04', '2018-09-06 00:01:04'),
(21, 'browse_settings', 'settings', '2018-09-06 00:01:04', '2018-09-06 00:01:04'),
(22, 'read_settings', 'settings', '2018-09-06 00:01:04', '2018-09-06 00:01:04'),
(23, 'edit_settings', 'settings', '2018-09-06 00:01:05', '2018-09-06 00:01:05'),
(24, 'add_settings', 'settings', '2018-09-06 00:01:05', '2018-09-06 00:01:05'),
(25, 'delete_settings', 'settings', '2018-09-06 00:01:05', '2018-09-06 00:01:05'),
(26, 'browse_categories', 'categories', '2018-09-06 00:01:34', '2018-09-06 00:01:34'),
(27, 'read_categories', 'categories', '2018-09-06 00:01:34', '2018-09-06 00:01:34'),
(28, 'edit_categories', 'categories', '2018-09-06 00:01:34', '2018-09-06 00:01:34'),
(29, 'add_categories', 'categories', '2018-09-06 00:01:34', '2018-09-06 00:01:34'),
(30, 'delete_categories', 'categories', '2018-09-06 00:01:35', '2018-09-06 00:01:35'),
(31, 'browse_posts', 'posts', '2018-09-06 00:01:41', '2018-09-06 00:01:41'),
(32, 'read_posts', 'posts', '2018-09-06 00:01:42', '2018-09-06 00:01:42'),
(33, 'edit_posts', 'posts', '2018-09-06 00:01:44', '2018-09-06 00:01:44'),
(34, 'add_posts', 'posts', '2018-09-06 00:01:44', '2018-09-06 00:01:44'),
(35, 'delete_posts', 'posts', '2018-09-06 00:01:46', '2018-09-06 00:01:46'),
(36, 'browse_pages', 'pages', '2018-09-06 00:02:05', '2018-09-06 00:02:05'),
(37, 'read_pages', 'pages', '2018-09-06 00:02:06', '2018-09-06 00:02:06'),
(38, 'edit_pages', 'pages', '2018-09-06 00:02:06', '2018-09-06 00:02:06'),
(39, 'add_pages', 'pages', '2018-09-06 00:02:07', '2018-09-06 00:02:07'),
(40, 'delete_pages', 'pages', '2018-09-06 00:02:07', '2018-09-06 00:02:07'),
(41, 'browse_hooks', NULL, '2018-09-06 00:02:23', '2018-09-06 00:02:23'),
(47, 'browse_room', 'room', '2018-09-08 05:43:38', '2018-09-08 05:43:38'),
(48, 'read_room', 'room', '2018-09-08 05:43:38', '2018-09-08 05:43:38'),
(49, 'edit_room', 'room', '2018-09-08 05:43:38', '2018-09-08 05:43:38'),
(50, 'add_room', 'room', '2018-09-08 05:43:38', '2018-09-08 05:43:38'),
(51, 'delete_room', 'room', '2018-09-08 05:43:38', '2018-09-08 05:43:38'),
(62, 'browse_bill_mgmt', 'bill_mgmt', '2018-09-08 07:41:13', '2018-09-08 07:41:13'),
(63, 'read_bill_mgmt', 'bill_mgmt', '2018-09-08 07:41:13', '2018-09-08 07:41:13'),
(64, 'edit_bill_mgmt', 'bill_mgmt', '2018-09-08 07:41:13', '2018-09-08 07:41:13'),
(65, 'add_bill_mgmt', 'bill_mgmt', '2018-09-08 07:41:13', '2018-09-08 07:41:13'),
(66, 'delete_bill_mgmt', 'bill_mgmt', '2018-09-08 07:41:14', '2018-09-08 07:41:14'),
(72, 'browse_billing', 'billing', '2018-09-16 22:20:16', '2018-09-16 22:20:16'),
(73, 'read_billing', 'billing', '2018-09-16 22:20:16', '2018-09-16 22:20:16'),
(74, 'edit_billing', 'billing', '2018-09-16 22:20:16', '2018-09-16 22:20:16'),
(75, 'add_billing', 'billing', '2018-09-16 22:20:16', '2018-09-16 22:20:16'),
(76, 'delete_billing', 'billing', '2018-09-16 22:20:16', '2018-09-16 22:20:16'),
(77, 'browse_billings', 'billings', '2018-09-16 22:32:59', '2018-09-16 22:32:59'),
(78, 'read_billings', 'billings', '2018-09-16 22:32:59', '2018-09-16 22:32:59'),
(79, 'edit_billings', 'billings', '2018-09-16 22:32:59', '2018-09-16 22:32:59'),
(80, 'add_billings', 'billings', '2018-09-16 22:32:59', '2018-09-16 22:32:59'),
(81, 'delete_billings', 'billings', '2018-09-16 22:32:59', '2018-09-16 22:32:59'),
(82, 'browse_rooms', 'rooms', '2018-09-17 05:57:53', '2018-09-17 05:57:53'),
(83, 'read_rooms', 'rooms', '2018-09-17 05:57:53', '2018-09-17 05:57:53'),
(84, 'edit_rooms', 'rooms', '2018-09-17 05:57:53', '2018-09-17 05:57:53'),
(85, 'add_rooms', 'rooms', '2018-09-17 05:57:53', '2018-09-17 05:57:53'),
(86, 'delete_rooms', 'rooms', '2018-09-17 05:57:53', '2018-09-17 05:57:53'),
(87, 'browse_tenants', 'tenants', '2018-09-17 06:50:37', '2018-09-17 06:50:37'),
(88, 'read_tenants', 'tenants', '2018-09-17 06:50:37', '2018-09-17 06:50:37'),
(89, 'edit_tenants', 'tenants', '2018-09-17 06:50:37', '2018-09-17 06:50:37'),
(90, 'add_tenants', 'tenants', '2018-09-17 06:50:37', '2018-09-17 06:50:37'),
(91, 'delete_tenants', 'tenants', '2018-09-17 06:50:37', '2018-09-17 06:50:37'),
(92, 'browse_bill_history', 'bill_history', '2018-09-18 05:59:01', '2018-09-18 05:59:01'),
(93, 'read_bill_history', 'bill_history', '2018-09-18 05:59:01', '2018-09-18 05:59:01'),
(94, 'edit_bill_history', 'bill_history', '2018-09-18 05:59:01', '2018-09-18 05:59:01'),
(95, 'add_bill_history', 'bill_history', '2018-09-18 05:59:01', '2018-09-18 05:59:01'),
(96, 'delete_bill_history', 'bill_history', '2018-09-18 05:59:01', '2018-09-18 05:59:01'),
(102, 'browse_bill_histories', 'bill_histories', '2018-09-19 08:20:24', '2018-09-19 08:20:24'),
(103, 'read_bill_histories', 'bill_histories', '2018-09-19 08:20:24', '2018-09-19 08:20:24'),
(104, 'edit_bill_histories', 'bill_histories', '2018-09-19 08:20:24', '2018-09-19 08:20:24'),
(105, 'add_bill_histories', 'bill_histories', '2018-09-19 08:20:24', '2018-09-19 08:20:24'),
(106, 'delete_bill_histories', 'bill_histories', '2018-09-19 08:20:24', '2018-09-19 08:20:24'),
(107, 'browse_billing_categories', 'billing_categories', '2018-09-24 22:16:06', '2018-09-24 22:16:06'),
(108, 'read_billing_categories', 'billing_categories', '2018-09-24 22:16:06', '2018-09-24 22:16:06'),
(109, 'edit_billing_categories', 'billing_categories', '2018-09-24 22:16:06', '2018-09-24 22:16:06'),
(110, 'add_billing_categories', 'billing_categories', '2018-09-24 22:16:06', '2018-09-24 22:16:06'),
(111, 'delete_billing_categories', 'billing_categories', '2018-09-24 22:16:06', '2018-09-24 22:16:06'),
(112, 'browse_problems', 'problems', '2018-10-01 07:54:58', '2018-10-01 07:54:58'),
(113, 'read_problems', 'problems', '2018-10-01 07:54:58', '2018-10-01 07:54:58'),
(114, 'edit_problems', 'problems', '2018-10-01 07:54:58', '2018-10-01 07:54:58'),
(115, 'add_problems', 'problems', '2018-10-01 07:54:58', '2018-10-01 07:54:58'),
(116, 'delete_problems', 'problems', '2018-10-01 07:54:58', '2018-10-01 07:54:58'),
(117, 'browse_messages', 'messages', '2018-10-08 19:33:35', '2018-10-08 19:33:35'),
(118, 'read_messages', 'messages', '2018-10-08 19:33:35', '2018-10-08 19:33:35'),
(119, 'edit_messages', 'messages', '2018-10-08 19:33:35', '2018-10-08 19:33:35'),
(120, 'add_messages', 'messages', '2018-10-08 19:33:35', '2018-10-08 19:33:35'),
(121, 'delete_messages', 'messages', '2018-10-08 19:33:35', '2018-10-08 19:33:35'),
(122, 'browse_events', 'events', '2018-10-08 19:34:25', '2018-10-08 19:34:25'),
(123, 'read_events', 'events', '2018-10-08 19:34:25', '2018-10-08 19:34:25'),
(124, 'edit_events', 'events', '2018-10-08 19:34:25', '2018-10-08 19:34:25'),
(125, 'add_events', 'events', '2018-10-08 19:34:25', '2018-10-08 19:34:25'),
(126, 'delete_events', 'events', '2018-10-08 19:34:25', '2018-10-08 19:34:25'),
(127, 'browse_tenant_classification', 'tenant_classification', '2018-10-15 20:57:53', '2018-10-15 20:57:53'),
(128, 'read_tenant_classification', 'tenant_classification', '2018-10-15 20:57:53', '2018-10-15 20:57:53'),
(129, 'edit_tenant_classification', 'tenant_classification', '2018-10-15 20:57:53', '2018-10-15 20:57:53'),
(130, 'add_tenant_classification', 'tenant_classification', '2018-10-15 20:57:53', '2018-10-15 20:57:53'),
(131, 'delete_tenant_classification', 'tenant_classification', '2018-10-15 20:57:53', '2018-10-15 20:57:53'),
(132, 'browse_tenant_classifications', 'tenant_classifications', '2018-10-15 21:12:46', '2018-10-15 21:12:46'),
(133, 'read_tenant_classifications', 'tenant_classifications', '2018-10-15 21:12:46', '2018-10-15 21:12:46'),
(134, 'edit_tenant_classifications', 'tenant_classifications', '2018-10-15 21:12:46', '2018-10-15 21:12:46'),
(135, 'add_tenant_classifications', 'tenant_classifications', '2018-10-15 21:12:46', '2018-10-15 21:12:46'),
(136, 'delete_tenant_classifications', 'tenant_classifications', '2018-10-15 21:12:46', '2018-10-15 21:12:46'),
(137, 'browse_gen_inquiries', 'gen_inquiries', '2018-10-19 07:24:01', '2018-10-19 07:24:01'),
(138, 'read_gen_inquiries', 'gen_inquiries', '2018-10-19 07:24:01', '2018-10-19 07:24:01'),
(139, 'edit_gen_inquiries', 'gen_inquiries', '2018-10-19 07:24:01', '2018-10-19 07:24:01'),
(140, 'add_gen_inquiries', 'gen_inquiries', '2018-10-19 07:24:01', '2018-10-19 07:24:01'),
(141, 'delete_gen_inquiries', 'gen_inquiries', '2018-10-19 07:24:01', '2018-10-19 07:24:01'),
(142, 'browse_payments', 'payments', '2018-10-19 08:43:56', '2018-10-19 08:43:56'),
(143, 'read_payments', 'payments', '2018-10-19 08:43:56', '2018-10-19 08:43:56'),
(144, 'edit_payments', 'payments', '2018-10-19 08:43:56', '2018-10-19 08:43:56'),
(145, 'add_payments', 'payments', '2018-10-19 08:43:56', '2018-10-19 08:43:56'),
(146, 'delete_payments', 'payments', '2018-10-19 08:43:56', '2018-10-19 08:43:56'),
(147, 'browse_datavisualizations', 'datavisualizations', '2018-10-28 08:30:35', '2018-10-28 08:30:35'),
(148, 'read_datavisualizations', 'datavisualizations', '2018-10-28 08:30:35', '2018-10-28 08:30:35'),
(149, 'edit_datavisualizations', 'datavisualizations', '2018-10-28 08:30:35', '2018-10-28 08:30:35'),
(150, 'add_datavisualizations', 'datavisualizations', '2018-10-28 08:30:35', '2018-10-28 08:30:35'),
(151, 'delete_datavisualizations', 'datavisualizations', '2018-10-28 08:30:35', '2018-10-28 08:30:35'),
(152, 'browse_announcements', 'announcements', '2018-11-02 23:18:00', '2018-11-02 23:18:00'),
(153, 'read_announcements', 'announcements', '2018-11-02 23:18:00', '2018-11-02 23:18:00'),
(154, 'edit_announcements', 'announcements', '2018-11-02 23:18:00', '2018-11-02 23:18:00'),
(155, 'add_announcements', 'announcements', '2018-11-02 23:18:00', '2018-11-02 23:18:00'),
(156, 'delete_announcements', 'announcements', '2018-11-02 23:18:00', '2018-11-02 23:18:00');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(2, 1),
(2, 2),
(3, 1),
(3, 2),
(4, 2),
(5, 1),
(6, 1),
(6, 2),
(7, 1),
(7, 2),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(47, 1),
(47, 2),
(48, 1),
(48, 2),
(49, 1),
(49, 2),
(50, 1),
(50, 2),
(51, 1),
(62, 1),
(63, 1),
(65, 1),
(66, 1),
(72, 1),
(72, 2),
(73, 1),
(73, 2),
(74, 1),
(75, 1),
(75, 2),
(76, 1),
(77, 1),
(77, 2),
(78, 1),
(78, 2),
(79, 1),
(80, 1),
(80, 2),
(81, 1),
(82, 1),
(82, 2),
(83, 1),
(83, 2),
(84, 1),
(85, 1),
(86, 1),
(87, 1),
(87, 2),
(88, 1),
(88, 2),
(89, 1),
(90, 1),
(91, 1),
(92, 1),
(92, 2),
(93, 1),
(93, 2),
(96, 1),
(102, 1),
(102, 2),
(103, 1),
(103, 2),
(104, 1),
(105, 1),
(106, 1),
(107, 1),
(107, 2),
(108, 1),
(108, 2),
(109, 1),
(109, 2),
(110, 1),
(110, 2),
(111, 1),
(112, 1),
(112, 2),
(113, 1),
(113, 2),
(114, 1),
(115, 1),
(116, 1),
(117, 1),
(117, 2),
(118, 1),
(118, 2),
(119, 1),
(120, 1),
(120, 2),
(121, 1),
(122, 1),
(122, 2),
(123, 1),
(123, 2),
(124, 1),
(125, 1),
(126, 1),
(127, 1),
(127, 2),
(128, 1),
(128, 2),
(129, 1),
(130, 1),
(131, 1),
(132, 1),
(132, 2),
(133, 1),
(133, 2),
(134, 1),
(135, 1),
(136, 1),
(137, 1),
(137, 2),
(138, 1),
(138, 2),
(141, 1),
(142, 1),
(142, 2),
(143, 1),
(143, 2),
(146, 1),
(147, 1),
(148, 1),
(149, 1),
(150, 1),
(151, 1),
(152, 1),
(153, 1),
(154, 1),
(155, 1),
(156, 1);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `author_id`, `category_id`, `title`, `seo_title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `featured`, `created_at`, `updated_at`) VALUES
(1, 0, NULL, 'Lorem Ipsum Post', NULL, 'This is the excerpt for the Lorem Ipsum Post', '<p>This is the body of the lorem ipsum post</p>', 'posts/post1.jpg', 'lorem-ipsum-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2018-09-06 00:01:53', '2018-09-06 00:01:53'),
(2, 0, NULL, 'My Sample Post', NULL, 'This is the excerpt for the sample Post', '<p>This is the body for the sample post, which includes the body.</p>\n                <h2>We can use all kinds of format!</h2>\n                <p>And include a bunch of other stuff.</p>', 'posts/post2.jpg', 'my-sample-post', 'Meta Description for sample post', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2018-09-06 00:01:54', '2018-09-06 00:01:54'),
(3, 0, NULL, 'Latest Post', NULL, 'This is the excerpt for the latest post', '<p>This is the body for the latest post</p>', 'posts/post3.jpg', 'latest-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2018-09-06 00:01:55', '2018-09-06 00:01:55'),
(4, 0, NULL, 'Yarr Post', NULL, 'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.', '<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>', 'posts/post4.jpg', 'yarr-post', 'this be a meta descript', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2018-09-06 00:01:56', '2018-09-06 00:01:56');

-- --------------------------------------------------------

--
-- Table structure for table `problems`
--

CREATE TABLE `problems` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `complainant_id` int(11) DEFAULT NULL,
  `title` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `severity` int(11) DEFAULT NULL,
  `solved_status` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `problems`
--

INSERT INTO `problems` (`id`, `user_id`, `complainant_id`, `title`, `details`, `severity`, `solved_status`, `created_at`, `updated_at`) VALUES
(1, 1, 14, 'Payment always declined', 'Di tinatanggap yung payment', 3, '30%', '2018-10-01 08:15:00', '2018-10-19 08:57:33');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2018-09-06 00:01:03', '2018-09-06 00:01:03'),
(2, 'Cashier', 'Cashier', '2018-09-06 00:01:03', '2018-09-09 21:50:33');

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `floor_no` int(11) DEFAULT NULL,
  `capacity` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id` int(10) UNSIGNED NOT NULL,
  `room_no` text COLLATE utf8mb4_unicode_ci,
  `status` text COLLATE utf8mb4_unicode_ci,
  `free_slots` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`floor_no`, `capacity`, `created_at`, `updated_at`, `id`, `room_no`, `status`, `free_slots`) VALUES
(1, 4, '2018-09-16 23:29:21', '2018-10-28 03:52:02', 5, '1A', 'FULL', 0),
(1, 6, '2018-09-16 23:29:47', '2018-10-28 00:30:50', 7, '1B', 'AVAILABLE', 4),
(2, 6, '2018-09-17 05:58:32', '2018-10-28 00:30:16', 8, '2A', 'AVAILABLE', 6),
(2, 4, '2018-09-17 05:58:56', '2018-10-28 00:29:35', 9, '2B', 'AVAILABLE', 3);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', 'settings\\October2018\\gWJt737PCjZ3bKmTAMSN.png', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', 'settings\\September2018\\oElOmOIPunYa2yGMso2r.jpg', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'PAYPAD', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'UPAD\'s Billing System', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', 'settings\\September2018\\BvnhMMgPtuOH6D30Vspi.gif', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', 'settings\\September2018\\TzS1LrLpNFJqx1fqUoms.png', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `tenants`
--

CREATE TABLE `tenants` (
  `id` int(10) UNSIGNED NOT NULL,
  `lastname` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `firstname` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `middlename` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birthdate` date NOT NULL,
  `email` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cellNo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `homeAddress` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `startofcontract` date NOT NULL,
  `endofcontract` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `username` varchar(103) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` char(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `room_id` int(11) DEFAULT NULL,
  `nickname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nationality` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `birthPlace` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `religion` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sex` char(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `civilStatus` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `landline` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mailingAddress` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `secondarySchool` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `college` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `course` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `collegeYear` int(11) DEFAULT NULL,
  `fatherName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fatherOccupation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fatherOffice` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fatherOfficeTel` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fatherOfficeCel` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fatherEmail` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `motherName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `motherOccupation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `motherOffice` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `motherOfficeTel` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `motherOfficeCel` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `motherEmail` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guardianName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guardianRelation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guardianAge` int(11) DEFAULT NULL,
  `guardianAddress` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guardianEmail` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guardianCel` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guardianTel` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guardianOccupation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emergencyContName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emergencyContNo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signatoryName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signatoryAddress` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contractLength` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `classification` text COLLATE utf8mb4_unicode_ci,
  `picture` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `monthly_rent` decimal(10,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tenants`
--

INSERT INTO `tenants` (`id`, `lastname`, `firstname`, `middlename`, `birthdate`, `email`, `cellNo`, `homeAddress`, `startofcontract`, `endofcontract`, `created_at`, `updated_at`, `username`, `password`, `room_id`, `nickname`, `nationality`, `age`, `birthPlace`, `religion`, `sex`, `civilStatus`, `landline`, `mailingAddress`, `secondarySchool`, `college`, `course`, `collegeYear`, `fatherName`, `fatherOccupation`, `fatherOffice`, `fatherOfficeTel`, `fatherOfficeCel`, `fatherEmail`, `motherName`, `motherOccupation`, `motherOffice`, `motherOfficeTel`, `motherOfficeCel`, `motherEmail`, `guardianName`, `guardianRelation`, `guardianAge`, `guardianAddress`, `guardianEmail`, `guardianCel`, `guardianTel`, `guardianOccupation`, `emergencyContName`, `emergencyContNo`, `signatoryName`, `signatoryAddress`, `contractLength`, `classification`, `picture`, `monthly_rent`) VALUES
(14, 'San Pedro', 'Mary Jane', 'G', '1997-01-01', 'mj@gmail.com', '09957451053', 'Cubao', '2018-01-01', '2020-01-01', '2018-09-17 06:57:00', '2018-10-27 21:38:24', '@mjsanpedro', 'password', 5, NULL, 'fasfa', 2, 'dfasfs', 'sdfasf', 'M', 'Single', '123123', 'fafaf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'sfasf', 'adsfasf', 2, 'sdfafa', 'asfsaf', '12', NULL, NULL, 'afasfas', '23', 'sdfaf', 'sfaf', '2', '2', NULL, '7000'),
(15, 'Siervo', 'Jason', 'Andarino', '1998-02-02', 'jason.siervo@gmail.com', '09567052663', 'Bulacan', '2018-02-02', '2020-02-02', '2018-09-17 07:04:00', '2018-10-27 23:54:21', '@jasiervo', 'password', 7, NULL, 'fafasf', 2, 'fasfa', '1', 'M', 'Single', '123123', 'fasfaf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'sasfaf', 'dfasfa', 2, 'adfasf', 'ssfaffa', '123', NULL, NULL, 'afasf', '1231', 'dfafa', '12312', '2', '2', NULL, '5000'),
(16, 'Palaming', 'Irish Kate', 'C', '1998-03-03', 'irish.palaming@gmail.com', '09750397909', 'Pangasinan', '2018-03-03', '2020-03-03', '2018-09-17 07:05:00', '2018-10-27 23:57:10', '@ipalaming', 'password', 5, NULL, 'sfasfa', 1, 'sfsaf', 'afaf', 'M', 'Single', '23213', 'dafa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'fafaf', 'dfasfa', 2, 'sfasfsa', 'safsaf', '2', NULL, NULL, 'safasfa', '123', 'sdfasfas', '2', '2', '2', NULL, '4000'),
(17, 'Ramos', 'Jerome', 'G', '1998-03-03', 'jgramos@gmail.com', '2', 'Sta. Mesa', '2016-09-09', '2020-09-09', '2018-09-18 06:41:00', '2018-10-27 06:16:45', '@jgramos', 'password', 9, NULL, 'fasfaf', 1, 'fasf', 'dfasfaf', 'M', 'Single', '3322', 'afadfads', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'afasfa', 'asfsdf', 2, 'asfafasf', 'adfaf', '2', NULL, NULL, 'dfasfaf', '2', 'afaf', '2', 'fafasf', '2', NULL, '10000'),
(18, 'Andarino', 'Kay Karen', 'Corollo', '1997-07-18', 'kay@gmail.com', '2', 'Capul', '2016-01-01', '2018-01-01', '2018-09-19 08:37:00', '2018-10-27 06:15:41', '@kcandarino', 'password', 5, NULL, 'fafasf', 2, 'fasfa', 'afaf', 'M', 'Single', '23123', 'safaf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'fafaf', 'fafaf', 2, 'fasfaf', 'fafaf', '2', NULL, NULL, 'fafaf', '12321', 'dfasfsdf', '21', '2', '2', NULL, '12000'),
(19, 'Siervo', 'Angelo Isaac', 'Andarino', '2000-07-19', 'feutechigniters@gmail.com', '13131', 'Bulacan', '2018-01-01', '2020-01-01', '2018-09-19 08:55:00', '2018-10-27 06:14:34', '@aasiervo', 'gettingwarmer', 7, NULL, 'afafaf', 2, 'afaf', 'faf', 'M', 'Single', '23131', 'fafaf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'fafaf', 'afaf', 2, 'afaf', 'afaf', '1231', NULL, NULL, 'afafa', '1313', 'afaf', '2313', '23', '2', NULL, '11000'),
(21, 'Siervo', 'Rosario', 'Andarino', '1998-10-11', 'jason.siervo@gmail.com', '09236801740', 'Bulacan', '2018-10-30', '2019-10-30', '2018-10-28 03:21:19', '2018-10-28 03:21:19', '@rasiervo', 'password', 5, 'Sarie', 'Philippines', 2, 'Capul', 'Catholic', 'F', 'Married', '2', 'Bulacan', 'Capul School', 'University of the Philippines', 'Accountancy', 2, 'Christian Siervo', 'Farmer', 'Bulacan', 'Bulacan', '09236801740', 'christian.siervo@gmail.com', 'Irish Siervo', 'None', 'Bulacan', 'Bulacan', '09236801740', 'irish.siervo@gmail.com', 'Christian Siervo', 'Father', 2, 'Bulacan', 'christian.siervo@gmail.com', '09236801740', 'Bulacan', 'Farmer', 'Christian Siervo', '09236801740', 'Rosario Siervo', 'Bulacan', '1', '2', NULL, '11000'),
(22, 'Siervo', 'Rosario', 'Andarino', '1998-10-11', 'jason.siervo@gmail.com', '09236801740', 'Bulacan', '2018-10-30', '2019-10-30', '2018-10-28 03:52:02', '2018-10-28 03:52:02', '@rasiervo', 'password', 5, 'Sarie', 'Philippines', 2, 'Capul', 'Catholic', 'F', 'Married', '2', 'Bulacan', 'Capul School', 'University of the Philippines', 'Accountancy', 2, 'Christian Siervo', 'Farmer', 'Bulacan', 'Bulacan', '09236801740', 'christian.siervo@gmail.com', 'Irish Siervo', 'None', 'Bulacan', 'Bulacan', '09236801740', 'irish.siervo@gmail.com', 'Christian Siervo', 'Father', 2, 'Bulacan', 'christian.siervo@gmail.com', '09236801740', 'Bulacan', 'Farmer', 'Christian Siervo', '09236801740', 'Rosario Siervo', 'Bulacan', '1', '2', NULL, '11000');

-- --------------------------------------------------------

--
-- Table structure for table `tenant_classifications`
--

CREATE TABLE `tenant_classifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `classification` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `short_description` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tenant_classifications`
--

INSERT INTO `tenant_classifications` (`id`, `classification`, `created_at`, `updated_at`, `short_description`) VALUES
(1, 'Incoming Tenant', '2018-10-15 21:16:55', '2018-10-15 21:16:55', NULL),
(2, 'Current Tenant', '2018-10-15 21:17:19', '2018-10-15 21:17:19', NULL),
(3, 'Departing Tenant', '2018-10-15 21:17:41', '2018-10-15 21:17:41', NULL),
(4, 'Former Tenant', '2018-10-15 21:18:10', '2018-10-15 21:18:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `translations`
--

INSERT INTO `translations` (`id`, `table_name`, `column_name`, `foreign_key`, `locale`, `value`, `created_at`, `updated_at`) VALUES
(1, 'data_types', 'display_name_singular', 5, 'pt', 'Post', '2018-09-06 00:02:11', '2018-09-06 00:02:11'),
(2, 'data_types', 'display_name_singular', 6, 'pt', 'Página', '2018-09-06 00:02:12', '2018-09-06 00:02:12'),
(3, 'data_types', 'display_name_singular', 1, 'pt', 'Utilizador', '2018-09-06 00:02:12', '2018-09-06 00:02:12'),
(4, 'data_types', 'display_name_singular', 4, 'pt', 'Categoria', '2018-09-06 00:02:15', '2018-09-06 00:02:15'),
(5, 'data_types', 'display_name_singular', 2, 'pt', 'Menu', '2018-09-06 00:02:17', '2018-09-06 00:02:17'),
(6, 'data_types', 'display_name_singular', 3, 'pt', 'Função', '2018-09-06 00:02:18', '2018-09-06 00:02:18'),
(7, 'data_types', 'display_name_plural', 5, 'pt', 'Posts', '2018-09-06 00:02:19', '2018-09-06 00:02:19'),
(8, 'data_types', 'display_name_plural', 6, 'pt', 'Páginas', '2018-09-06 00:02:19', '2018-09-06 00:02:19'),
(9, 'data_types', 'display_name_plural', 1, 'pt', 'Utilizadores', '2018-09-06 00:02:20', '2018-09-06 00:02:20'),
(10, 'data_types', 'display_name_plural', 4, 'pt', 'Categorias', '2018-09-06 00:02:20', '2018-09-06 00:02:20'),
(11, 'data_types', 'display_name_plural', 2, 'pt', 'Menus', '2018-09-06 00:02:20', '2018-09-06 00:02:20'),
(12, 'data_types', 'display_name_plural', 3, 'pt', 'Funções', '2018-09-06 00:02:20', '2018-09-06 00:02:20'),
(13, 'categories', 'slug', 1, 'pt', 'categoria-1', '2018-09-06 00:02:20', '2018-09-06 00:02:20'),
(14, 'categories', 'name', 1, 'pt', 'Categoria 1', '2018-09-06 00:02:20', '2018-09-06 00:02:20'),
(15, 'categories', 'slug', 2, 'pt', 'categoria-2', '2018-09-06 00:02:20', '2018-09-06 00:02:20'),
(16, 'categories', 'name', 2, 'pt', 'Categoria 2', '2018-09-06 00:02:20', '2018-09-06 00:02:20'),
(17, 'pages', 'title', 1, 'pt', 'Olá Mundo', '2018-09-06 00:02:20', '2018-09-06 00:02:20'),
(18, 'pages', 'slug', 1, 'pt', 'ola-mundo', '2018-09-06 00:02:21', '2018-09-06 00:02:21'),
(19, 'pages', 'body', 1, 'pt', '<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', '2018-09-06 00:02:21', '2018-09-06 00:02:21'),
(20, 'menu_items', 'title', 1, 'pt', 'Painel de Controle', '2018-09-06 00:02:21', '2018-09-06 00:02:21'),
(21, 'menu_items', 'title', 2, 'pt', 'Media', '2018-09-06 00:02:21', '2018-09-06 00:02:21'),
(22, 'menu_items', 'title', 12, 'pt', 'Publicações', '2018-09-06 00:02:21', '2018-09-06 00:02:21'),
(23, 'menu_items', 'title', 3, 'pt', 'Utilizadores', '2018-09-06 00:02:21', '2018-09-06 00:02:21'),
(24, 'menu_items', 'title', 11, 'pt', 'Categorias', '2018-09-06 00:02:21', '2018-09-06 00:02:21'),
(25, 'menu_items', 'title', 13, 'pt', 'Páginas', '2018-09-06 00:02:21', '2018-09-06 00:02:21'),
(26, 'menu_items', 'title', 4, 'pt', 'Funções', '2018-09-06 00:02:21', '2018-09-06 00:02:21'),
(27, 'menu_items', 'title', 5, 'pt', 'Ferramentas', '2018-09-06 00:02:21', '2018-09-06 00:02:21'),
(28, 'menu_items', 'title', 6, 'pt', 'Menus', '2018-09-06 00:02:21', '2018-09-06 00:02:21'),
(29, 'menu_items', 'title', 7, 'pt', 'Base de dados', '2018-09-06 00:02:21', '2018-09-06 00:02:21'),
(30, 'menu_items', 'title', 10, 'pt', 'Configurações', '2018-09-06 00:02:21', '2018-09-06 00:02:21');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(1, 1, 'Admin', 'admin@admin.com', 'users\\September2018\\WGHJjl8hqhhFPLHUG1op.png', '$2y$10$gCK0R2nZ1sxyCPAFoGDiEuIN06FJIJKC.vu.yUTLJN0KzUOptdof2', 'yDVkcu6RbefpJk03ukVLy8XCMcHbWufL2g6NGhHo0WwpvBI3LkxqCizN6ppA', '{\"locale\":\"en\"}', '2018-09-06 00:01:36', '2018-10-16 03:25:12'),
(2, 2, 'Jason Siervo', 'jason@gmail.com', 'users\\October2018\\ybDJV0kualBI9Fpx0ojv.png', '$2y$10$ESEq.m.dPR74uGTgcbfljex5SpPBG8QUr0ObMa4YhOFw.rprvqmce', 'zRWAOkjrbxVzd0VfNVysGppNsStCvd5BQuGcq8Xq4GGtBhmYy1Qnr69JL6bl', '{\"locale\":\"en\"}', '2018-09-09 21:51:15', '2018-10-27 01:10:09');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `announcements`
--
ALTER TABLE `announcements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `billings`
--
ALTER TABLE `billings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `billing_categories`
--
ALTER TABLE `billing_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bill_histories`
--
ALTER TABLE `bill_histories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`),
  ADD KEY `categories_parent_id_foreign` (`parent_id`);

--
-- Indexes for table `datavisualizations`
--
ALTER TABLE `datavisualizations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gen_inquiries`
--
ALTER TABLE `gen_inquiries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_slug_unique` (`slug`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `posts_slug_unique` (`slug`);

--
-- Indexes for table `problems`
--
ALTER TABLE `problems`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `tenants`
--
ALTER TABLE `tenants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tenant_classifications`
--
ALTER TABLE `tenant_classifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `announcements`
--
ALTER TABLE `announcements`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `billings`
--
ALTER TABLE `billings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=171;

--
-- AUTO_INCREMENT for table `billing_categories`
--
ALTER TABLE `billing_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `bill_histories`
--
ALTER TABLE `bill_histories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=358;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `datavisualizations`
--
ALTER TABLE `datavisualizations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=305;

--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `gen_inquiries`
--
ALTER TABLE `gen_inquiries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=157;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `problems`
--
ALTER TABLE `problems`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tenants`
--
ALTER TABLE `tenants`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `tenant_classifications`
--
ALTER TABLE `tenant_classifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
