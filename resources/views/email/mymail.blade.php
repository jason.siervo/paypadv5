<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title></title>
</head>
<body>

    

    <center>
        <h1>Billing Statement 
        
        <?php
            // if($bill_type == 1){
            //     echo "- Electricity";
            // } elseif($bill_type == 2){
            //     echo "- Water";
            // } else {
            //     echo "";
            // }
        ?>
        
        </h1>

        <h3>University Pad Residences</h3>
        <h3>861 Padre Campa St., Sampaloc, Manila, 1008</h3>
        <br>
        <br>

        <?php //dd($inputs); ?>
    

        <p><b>Name:</b> {{$inputs['lastname']}}, {{$inputs['firstname']}} {{$inputs['middlename']}}</p>
        <p><b>Billing Period:</b> {{$inputs['start_date']}} - {{$inputs['end_date']}} </p>
        <p><b>Due Date:</b> {{$inputs['due_date']}} </p>
        <br>
        <p><b>Amount:</b> {{$inputs['amount']}}</p>
        <br>
        <br>
    
        <p><b>Note:</b> Please pay the amount required for this bill, payment made after the<br>
                        due date will have an addditional charge of 3% based on the original amount.
        </p>
 
    <br>
    <br>
    <p>__________________________________________________________________________________________<p>

        <p><b>Got any questions? </b></p><br>
        <p> Telephone No: (+632) 7427485, (+632) 7427486, (+632) 7427491 </p>
        <p> Mobile: 0917-7981437 , 0917-7981438 </p>
        <p> Website: universitypaypad.com </p>

    </center>
    
    
</body>
</html>