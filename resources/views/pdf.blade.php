<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title></title>
</head>
<body>

    

    <center>
        <h1>Billing Statement 
        
        <?php
            if($pdfPrint->bill_type == 1){
                echo "- Electricity";
            } elseif($pdfPrint->bill_type == 2){
                echo "- Water";
            } else {
                echo "";
            }
            
        ?>
        
        </h1>

        <h3>University Pad Residences</h3>
        <h3>861 Padre Campa St., Sampaloc, Manila, 1008</h3>
        <br>
        <br>
    

        <p><b>Name:</b> {{$pdfPrint->lastname}}, {{$pdfPrint->firstname}}</p>
        <p><b>Billing Period:</b> {{$pdfPrint->start_date}} - {{$pdfPrint->end_date}} </p>
        <p><b>Due Date:</b> {{$pdfPrint->due_date}} </p>
        <br>
        <p><b>Amount:</b> {{$pdfPrint->amount}}</p>
        <br>
        <br>
    
        <p><b>Note:</b> Please pay the amount required for this bill, payment made after the<br>
                        due date will have an addditional charge of 3% based on the original amount.
        </p>
    </center>
    <br>
    <br>
    <p>__________________________________________________________________________________________<p>
    <center>
        <p><b>Got any questions? </b></p><br>
        <p> Telephone No: (+632) 7427485, (+632) 7427486, (+632) 7427491 </p>
        <p> Mobile: 0917-7981437 , 0917-7981438 </p>
        <p> Website: universitypaypad.com </p>

    </center>
    
    
</body>
</html>