@include('header')

	
	<div id="fh5co-services-section">
		<div class="container">
			<div class="row">
				
				<div class="col-md-6 col-md-offset-3 text-center fh5co-heading animate-box">
					<h2>Our Awesome Services</h2>
					<p>Does leaving home for the first time feel like a little more than you can handle? Check out for our awesome services below! </p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 animate-box">
					<div class="services">
						<i class="icon-lock"></i>
						<div class="desc">
							<h3>Safety</h3>
							<p>Fully equipped 24/7security through 80 CCTV cameras, roving guards and a RFID Card Access security system for the entrance door and their respective room.</p>							
		
							</div>
					</div>
				</div>
				<div class="col-md-4 animate-box">
					<div class="services">
						<i class="icon-check"></i>
						<div class="desc">
							<h3>Conduciveness</h3>
							<p>Conducive environment such as study area, private bath, refrigerator, a sink to wash utensils, and of course a microwave.</p>
							</div>
					</div>
				</div>
				<div class="col-md-4 animate-box">
					<div class="services">
						<i class="icon-laptop"></i>
						<div class="desc">
							<h3>Convenience</h3>
							<p>The latest technological amenities to make access to information faster and a lot easier such as wireless Internet access, telephone and television with cable connection to know the latest news.</p>						</div>
					</div>
				</div>
				<div class="col-md-4 animate-box">
					<div class="services">
						<i class="icon-server"></i>
						<div class="desc">
							<h3>Technologically advanced</h3>
							<p>Each tenants can access on our own Mobile Application available on both android and IOS. On the said App, they can easily keep track of their bills and can easily drop message to the mangement if they have suggestions and concerns.</p>
						</div>
					</div>
				</div>
				<div class="col-md-4 animate-box">
					<div class="services">
						<i class="icon-bed"></i>
						<div class="desc">
							<h3>Comfortability</h3>
							<!-- <h3>Online Marketing</h3> -->
							<p>Hotel-liked dormitory in Manila that is worth checking. Fully furnished modern rooms where the student will be staying and retaining the feeling of home.</p>
						</div>
					</div>
				</div>
				<div class="col-md-4 animate-box">
					<div class="services">
						<i class="icon-trash"></i>
						<div class="desc">
						<h3>Cleanliness</h3>
							<!-- <h3>Business Analytics</h3> -->
							<p>House keepers are just one call away to maintain cleanliness in their respective rooms.</p>
						</div>
					</div>
				</div>
			</div>

			<div class="row fh5co-services">
				<!-- <div class="col-md-6 col-md-offset-3 text-center fh5co-heading animate-box">
					<h2>Our pleasure to serve you</h2>
					<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
				</div>
				<div class="col-md-4 text-center item-block animate-box">
					<h3>Strategy</h3>
					<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove.</p>
					<p><a href="#" class="btn btn-primary btn-outline with-arrow">Learn more <i class="icon-arrow-right"></i></a></p>
				</div>
				<div class="col-md-4 text-center item-block animate-box">
					<h3>Explore</h3>
					<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove.</p>
					<p><a href="#" class="btn btn-primary btn-outline with-arrow">Learn more <i class="icon-arrow-right"></i></a></p>
				</div>
				<div class="col-md-4 text-center item-block animate-box">
					<h3>Expertise</h3>
					<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove.</p>
					<p><a href="#" class="btn btn-primary btn-outline with-arrow">Learn more <i class="icon-arrow-right"></i></a></p>
				</div> -->
			</div>
		</div>
	</div>

	<div id="fh5co-pricing-section" class="fh5co-light-grey-section">
	<br><br>
	<center><img src="images/flag1.png" width="800" height="180"></center>

		<div class="container">

			<div class="row">
				<div class="col-md-6 col-md-offset-3 text-center fh5co-heading animate-box">
				<div class="price-box popular" style="background-image: url(images/blue1.gif);">
				
					<h2>Start the semester right with #YourNextHome</h2>
					</div>
					<p>The only choice for convenient and affordable luxury student living, University Pad Residences is just a few steps away from the country's best universities, making it your home away from home.</p>
				</div>
			
			</div>
		</div>
	</div>
<br>
<center>
	<span><h3>R O O M  T Y P E S &nbsp;<i class="icon-bed"></i></h3></span>
</center>
	<div id="fh5co-work-section" class="fh5co-light-grey-section">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3 text-center fh5co-heading animate-box">
				
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 animate-box">
					<a href="#" class="item-grid text-center">
						<div class="image" style="background-image: url(images/room6.png)"></div>
						<div class="v-align">
							<div class="v-align-middle">
								<h3 class="title">Hexatruple Sharing</h3>
								<h5 class="category"> Free Wifi | Own Cabinet | Own Study Table</h5>
							</div>
						</div>
					</a>
				</div>
				<div class="col-md-4 animate-box">
					<a href="#" class="item-grid text-center">
						<div class="image" style="background-image: url(images/rooms.png)"></div>
						<div class="v-align">
							<div class="v-align-middle">
								<h3 class="title">Quadruple Sharing</h3>
								<h5 class="category"> Free Wifi | Own Cabinet | Own Study Table</h5>
							</div>
						</div>
					</a>
				</div>
				<div class="col-md-4 animate-box">
					<a href="#" class="item-grid text-center">
						<div class="image" style="background-image: url(images/rooms2.png)"></div>
						<div class="v-align">
							<div class="v-align-middle">
								<h3 class="title">double Sharing</h3>
								<h5 class="category"> Free Wifi | Own Cabinet | Own Study Table</h5>
							</div>
						</div>
					</a>
				</div>
				
				<div class="col-md-12 text-center animate-box">
					<!-- <p><a href="#" class="btn btn-primary with-arrow">View More Projects <i class="icon-arrow-right"></i></a></p> -->
				</div>
			</div>
		</div>
	</div>

	<div class="fh5co-cta" style="background-image: url(images/upad.jpg);">
		<div class="overlay"></div>
		<div class="container">
			<div class="col-md-12 text-center animate-box">
			<p><font size="60">SAFE. SECURE. CONVENIENT. AFFORDABLE.</font></p>
				<!-- <p><a href="#" class="btn btn-primary btn-outline with-arrow">Get started now! <i class="icon-arrow-right"></i></a></p> -->
			</div>
		</div>
	</div>

	
	
	@include('footer')