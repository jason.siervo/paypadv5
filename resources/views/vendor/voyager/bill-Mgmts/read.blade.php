@extends('voyager::master')

@section('page_title', __('voyager::generic.view').' '.$dataType->display_name_singular)

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i> {{ __('voyager::generic.viewing') }} {{ ucfirst($dataType->display_name_singular) }} &nbsp;

        @can('edit', $dataTypeContent)
        <a href="{{ route('voyager.'.$dataType->slug.'.edit', $dataTypeContent->getKey()) }}" class="btn btn-info">
            <span class="glyphicon glyphicon-pencil"></span>&nbsp;
            {{ __('voyager::generic.edit') }}
        </a>
        @endcan
        @can('delete', $dataTypeContent)
            <a href="javascript:;" title="{{ __('voyager::generic.delete') }}" class="btn btn-danger delete" data-id="{{ $dataTypeContent->getKey() }}" id="delete-{{ $dataTypeContent->getKey() }}">
                <i class="voyager-trash"></i> <span class="hidden-xs hidden-sm">{{ __('voyager::generic.delete') }}</span>
            </a>
        @endcan

        <a href="{{ route('voyager.'.$dataType->slug.'.index') }}" class="btn btn-warning">
            <span class="glyphicon glyphicon-list"></span>&nbsp;
            {{ __('voyager::generic.return_to_list') }}
        </a>
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content read container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="col-md-4">
                    <div class="panel panel-bordered" style="padding-bottom:5px;">
                        <!-- form start -->
                        @foreach($dataType->readRows as $row)
                            @php $rowDetails = json_decode($row->details);
                            if($rowDetails === null){
                                    $rowDetails=new stdClass();
                                    $rowDetails->options=new stdClass();
                            }
                            @endphp
                            
                            
                                
                            
                            <div class="panel-heading" style="border-bottom:0;">
                                <h3 class="panel-title">{{ $row->display_name }}</h3>
                            </div>

                            <div class="panel-body" style="padding-top:0;">
                                @if($row->type == "image")
                                    <img class="img-responsive"
                                        src="{{ filter_var($dataTypeContent->{$row->field}, FILTER_VALIDATE_URL) ? $dataTypeContent->{$row->field} : Voyager::image($dataTypeContent->{$row->field}) }}">
                                @elseif($row->type == 'multiple_images')
                                    @if(json_decode($dataTypeContent->{$row->field}))
                                        @foreach(json_decode($dataTypeContent->{$row->field}) as $file)
                                            <img class="img-responsive"
                                                src="{{ filter_var($file, FILTER_VALIDATE_URL) ? $file : Voyager::image($file) }}">
                                        @endforeach
                                    @else
                                        <img class="img-responsive"
                                            src="{{ filter_var($dataTypeContent->{$row->field}, FILTER_VALIDATE_URL) ? $dataTypeContent->{$row->field} : Voyager::image($dataTypeContent->{$row->field}) }}">
                                    @endif
                                @elseif($row->type == 'relationship')
                                    @include('voyager::formfields.relationship', ['view' => 'read', 'options' => $rowDetails])
                                @elseif($row->type == 'select_dropdown' && property_exists($rowDetails, 'options') &&
                                        !empty($rowDetails->options->{$dataTypeContent->{$row->field}})
                                )

                                    <?php echo $rowDetails->options->{$dataTypeContent->{$row->field}};?>
                                @elseif($row->type == 'select_dropdown' && $dataTypeContent->{$row->field . '_page_slug'})
                                    <a href="{{ $dataTypeContent->{$row->field . '_page_slug'} }}">{{ $dataTypeContent->{$row->field}  }}</a>
                                @elseif($row->type == 'select_multiple')
                                    @if(property_exists($rowDetails, 'relationship'))

                                        @foreach(json_decode($dataTypeContent->{$row->field}) as $item)
                                            @if($item->{$row->field . '_page_slug'})
                                            <a href="{{ $item->{$row->field . '_page_slug'} }}">{{ $item->{$row->field}  }}</a>@if(!$loop->last), @endif
                                            @else
                                            {{ $item->{$row->field}  }}
                                            @endif
                                        @endforeach

                                    @elseif(property_exists($rowDetails, 'options'))
                                        @foreach(json_decode($dataTypeContent->{$row->field}) as $item)
                                        {{ $rowDetails->options->{$item} . (!$loop->last ? ', ' : '') }}
                                        @endforeach
                                    @endif
                                @elseif($row->type == 'date' || $row->type == 'timestamp')
                                    {{ $rowDetails && property_exists($rowDetails, 'format') ? \Carbon\Carbon::parse($dataTypeContent->{$row->field})->formatLocalized($rowDetails->format) : $dataTypeContent->{$row->field} }}
                                @elseif($row->type == 'checkbox')
                                    @if($rowDetails && property_exists($rowDetails, 'on') && property_exists($rowDetails, 'off'))
                                        @if($dataTypeContent->{$row->field})
                                        <span class="label label-info">{{ $rowDetails->on }}</span>
                                        @else
                                        <span class="label label-primary">{{ $rowDetails->off }}</span>
                                        @endif
                                    @else
                                    {{ $dataTypeContent->{$row->field} }}
                                    @endif
                                @elseif($row->type == 'color')
                                    <span class="badge badge-lg" style="background-color: {{ $dataTypeContent->{$row->field} }}">{{ $dataTypeContent->{$row->field} }}</span>
                                @elseif($row->type == 'coordinates')
                                    @include('voyager::partials.coordinates')
                                @elseif($row->type == 'rich_text_box')
                                    @include('voyager::multilingual.input-hidden-bread-read')
                                    <p>{!! $dataTypeContent->{$row->field} !!}</p>
                                @elseif($row->type == 'file')
                                    @if(json_decode($dataTypeContent->{$row->field}))
                                        @foreach(json_decode($dataTypeContent->{$row->field}) as $file)
                                            <a href="{{ Storage::disk(config('voyager.storage.disk'))->url($file->download_link) ?: '' }}">
                                                {{ $file->original_name ?: '' }}
                                            </a>
                                            <br/>
                                        @endforeach
                                    @else
                                        <a href="{{ Storage::disk(config('voyager.storage.disk'))->url($row->field) ?: '' }}">
                                            {{ __('voyager::generic.download') }}
                                        </a>
                                    @endif
                                @else
                                    @include('voyager::multilingual.input-hidden-bread-read')
                                    <p>{{ $dataTypeContent->{$row->field} }}</p>
                                @endif
                            </div><!-- panel-body -->
                            @if(!$loop->last)
                                <hr style="margin:0;">
                            @endif
                        @endforeach
                    </div>
                </div>

                {{-- gawa ko na table --}}
                {{-- Start of table --}}
                <div class="col-md-8" style="background-color: white">
                    <h3 class="panel-title">Room Occupants</h3>
                    <table id="dataTable" class="table table-hover">
                        <thead>
                            <tr>
                                <th>
                                    <input type="checkbox" class="select_all">
                                </th>
                                <th>
                                    {{-- <a href="http://127.0.0.1:8000/admin/bill-mgmts?sort_order=desc&amp;order_by=billMgmt_id"> --}}
                                        Last Name
                                    </a>
                                </th>
                                <th>
                                    <!-- <a href="http://127.0.0.1:8000/admin/bill-mgmts?sort_order=desc&amp;order_by=bill_mgmt_belongsto_room_relationship">  -->
                                        First Name
                                    <!-- </a> -->
                                </th>
                                <th>Middle Name</th>
                                <th>Email</th>
                                <th>Contact no.</th>
                                <th class="actions text-right">Actions</th>
                              
                            </tr>
                        </thead>

                        {{-- foreach loop para madisplay lahat ng tenants sa room --}}
                        <?php $tenant = $tenants->where('id', $bills->id) ?>
                        @foreach($tenant as $eachTenant)
                            <tbody>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="row_id" id="checkbox_4" value="4">
                                    </td>
                                    <td> 
                                        <p>{{ $eachTenant->lastname }}</p>
                                    </td>
                                    <td>
                                        <p>{{ $eachTenant->firstname }}</p>
                                    </td>
                                    <td>
                                        <p>{{ $eachTenant->middlename }}</p>
                                    </td>
                                    <td>
                                        <p>{{ $eachTenant->email }}</p>
                                    </td>
                                    <td>
                                        <p>{{ $eachTenant->contactno }}</p>
                                    </td>
                                    <td class="no-sort no-click" id="bread-actions">
                                        <!-- <a href="javascript:;" title="Delete" class="btn btn-sm btn-danger pull-right delete" data-id="4" id="delete-4">
                                            <i class="voyager-trash"></i> <span class="hidden-xs hidden-sm">Delete</span>
                                        </a>
                                        <a href="http://127.0.0.1:8000/admin/bill-mgmts/4/edit" title="Edit" class="btn btn-sm btn-primary pull-right edit">
                                            <i class="voyager-edit"></i> <span class="hidden-xs hidden-sm">Edit</span>
                                        </a> -->
                                        <a href="{{ url('read') }}" title="View" class="btn btn-sm btn-warning pull-right view">
                                            <i class="voyager-eye"></i> <span class="hidden-xs hidden-sm">View Bills</span>
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        @endforeach
                        
                    </table>

                    
                </div>
                {{-- End of Table --}}

                
<!-- add -->

 <div class="col-md-2" style="background-color: white">
                    <h3 class="panel-title">Electricity</h3>
                    <table id="dataTable" class="table table-hover">
                        <thead>
                            <tr>
                                <th>
                                    <input type="checkbox" class="select_all">
                                </th>
                                <th>
                                    {{-- <a href="http://127.0.0.1:8000/admin/bill-mgmts?sort_order=desc&amp;order_by=billMgmt_id"> --}}
                                        Last Name
                                    </a>
                                </th>
                                <th>
                                    {{-- <a href="http://127.0.0.1:8000/admin/bill-mgmts?sort_order=desc&amp;order_by=bill_mgmt_belongsto_room_relationship"> --}}
                                        First Name
                                    </a>
                                </th>
                             
                            </tr>
                        </thead>

                         {{-- foreach loop para madisplay lahat ng tenants sa room --}}
                        <?php $tenant = $tenants->where('room_id', $bills->room_id) ?>
                        @foreach($tenant as $eachTenant)
                            <tbody>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="row_id" id="checkbox_4" value="4">
                                    </td>
                                    <td>
                                        {{-- <div class="readmore" style="max-height: none;">B1C</div> --}}
                                        <p>{{ $eachTenant->lastname }}</p>
                                    </td>
                                    <td>
                                        <p>{{ $eachTenant->firstname }}</p>
                                    </td>

                                        
                                </tr>
                            </tbody>
                        @endforeach

                        </table>
                        
                <div class="form-group">
                    <label for="usr">Amount:</label>
                    <input type="number" class="form-control" id="usr">
                </div>

            <a href="" title="Add" class="btn btn-sm btn-success pull-right view">
                <i class="voyager-plus"></i> <span class="hidden-xs hidden-sm">Add Bill</span>
            </a>
</div>

                          
                        
                   
<!--end-->
              
<!-- add -->

 <div class="col-md-2" style="background-color: white">
                    <h3 class="panel-title">Water</h3>
                    <table id="dataTable" class="table table-hover">
                        <thead>
                            <tr>
                                <th>
                                    <input type="checkbox" class="select_all">
                                </th>
                                <th>
                                    {{-- <a href="http://127.0.0.1:8000/admin/bill-mgmts?sort_order=desc&amp;order_by=billMgmt_id"> --}}
                                        Last Name
                                    </a>
                                </th>
                                <th>
                                    {{-- <a href="http://127.0.0.1:8000/admin/bill-mgmts?sort_order=desc&amp;order_by=bill_mgmt_belongsto_room_relationship"> --}}
                                        First Name
                                    </a>
                                </th>
                             
                            </tr>
                        </thead>
                        {{-- foreach loop para madisplay lahat ng tenants sa room --}}
                        <?php $tenant = $tenants->where('room_id', $bills->room_id) ?>
                        @foreach($tenant as $eachTenant)
                            <tbody>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="row_id" id="checkbox_4" value="4">
                                    </td>
                                    <td>
                                        {{-- <div class="readmore" style="max-height: none;">B1C</div> --}}
                                        <p>{{ $eachTenant->lastname }}</p>
                                    </td>
                                    <td>
                                        <p>{{ $eachTenant->firstname }}</p>
                                    </td>

                                        
                                </tr>
                            </tbody>
                        @endforeach

                        </table>
                        
                <div class="form-group">
                    <label for="usr">Amount:</label>
                    <input type="number" class="form-control" id="usr">
                </div>

            <a href="" title="Add" class="btn btn-sm btn-success pull-right view">
                <i class="voyager-plus"></i> <span class="hidden-xs hidden-sm">Add Bill</span>
            </a>
</div>

                          
                        
                   
<!--end-->
              
<!-- add -->

 <div class="col-md-2" style="background-color: white">
                    <h3 class="panel-title">Parking</h3>
                    <table id="dataTable" class="table table-hover">
                        <thead>
                            <tr>
                                <th>
                                    <input type="checkbox" class="select_all">
                                </th>
                                <th>
                                    {{-- <a href="http://127.0.0.1:8000/admin/bill-mgmts?sort_order=desc&amp;order_by=billMgmt_id"> --}}
                                        Last Name
                                    </a>
                                </th>
                                <th>
                                    {{-- <a href="http://127.0.0.1:8000/admin/bill-mgmts?sort_order=desc&amp;order_by=bill_mgmt_belongsto_room_relationship"> --}}
                                        First Name
                                    </a>
                                </th>
                             
                            </tr>
                        </thead>
                        {{-- foreach loop para madisplay lahat ng tenants sa room --}}
                        <?php $tenant = $tenants->where('room_id', $bills->room_id) ?>
                        @foreach($tenant as $eachTenant)
                            <tbody>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="row_id" id="checkbox_4" value="4">
                                    </td>
                                    <td>
                                        {{-- <div class="readmore" style="max-height: none;">B1C</div> --}}
                                        <p>{{ $eachTenant->lastname }}</p>
                                    </td>
                                    <td>
                                        <p>{{ $eachTenant->firstname }}</p>
                                    </td>

                                        
                                </tr>
                            </tbody>
                        @endforeach

                        </table>
                        
                <div class="form-group">
                    <label for="usr">Amount:</label>
                    <input type="number" class="form-control" id="usr">
                </div>

            <a href="" title="Add" class="btn btn-sm btn-success pull-right view">
                <i class="voyager-plus"></i> <span class="hidden-xs hidden-sm">Add Bill</span>
            </a>
</div>

                          
                        
                   
<!--end-->
              
<!-- add -->

 <div class="col-md-2" style="background-color: white">
                    <h3 class="panel-title">Electricity</h3>
                    <table id="dataTable" class="table table-hover">
                        <thead>
                            <tr>
                                <th>
                                    <input type="checkbox" class="select_all">
                                </th>
                                <th>
                                    {{-- <a href="http://127.0.0.1:8000/admin/bill-mgmts?sort_order=desc&amp;order_by=billMgmt_id"> --}}
                                        Last Name
                                    </a>
                                </th>
                                <th>
                                    {{-- <a href="http://127.0.0.1:8000/admin/bill-mgmts?sort_order=desc&amp;order_by=bill_mgmt_belongsto_room_relationship"> --}}
                                        First Name
                                    </a>
                                </th>
                             
                            </tr>
                        </thead>
                        {{-- foreach loop para madisplay lahat ng tenants sa room --}}
                        <?php $tenant = $tenants->where('room_id', $bills->room_id) ?>
                        @foreach($tenant as $eachTenant)
                            <tbody>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="row_id" id="checkbox_4" value="4">
                                    </td>
                                    <td>
                                        {{-- <div class="readmore" style="max-height: none;">B1C</div> --}}
                                        <p>{{ $eachTenant->lastname }}</p>
                                    </td>
                                    <td>
                                        <p>{{ $eachTenant->firstname }}</p>
                                    </td>

                                        
                                </tr>
                            </tbody>
                        @endforeach

                        </table>
                        
                <div class="form-group">
                    <label for="usr">Amount:</label>
                    <input type="number" class="form-control" id="usr">
                </div>

            <a href="" title="Add" class="btn btn-sm btn-success pull-right view">
                <i class="voyager-plus"></i> <span class="hidden-xs hidden-sm">Add Bill</span>
            </a>
</div>

                          
                        
                   
<!--end-->

     
            </div>

       
       
        </div>
        
    </div>

         
    {{-- Single delete modal --}}
    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }} {{ strtolower($dataType->display_name_singular) }}?</h4>
                </div>
                <div class="modal-footer">
                    <form action="{{ route('voyager.'.$dataType->slug.'.index') }}" id="delete_form" method="POST">
                        {{ method_field("DELETE") }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger pull-right delete-confirm"
                               value="{{ __('voyager::generic.delete_confirm') }} {{ strtolower($dataType->display_name_singular) }}">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop

@section('javascript')
    @if ($isModelTranslatable)
    <script>
        $(document).ready(function () {
            $('.side-body').multilingual();
        });
    </script>
    <script src="{{ voyager_asset('js/multilingual.js') }}"></script>
    @endif
    <script>
        var deleteFormAction;
        $('.delete').on('click', function (e) {
            var form = $('#delete_form')[0];

            if (!deleteFormAction) { // Save form action initial value
                deleteFormAction = form.action;
            }

            form.action = deleteFormAction.match(/\/[0-9]+$/)
                ? deleteFormAction.replace(/([0-9]+$)/, $(this).data('id'))
                : deleteFormAction + '/' + $(this).data('id');
            console.log(form.action);

            $('#delete_modal').modal('show');
        });

    </script>
@stop
