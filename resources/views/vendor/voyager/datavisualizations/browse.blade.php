@extends('voyager::master')


@section('content')

    <!-- chart -->
<BR><BR>
      <div class="row">
        <div class="col-sm-8"> <div id="columnchart_values" ></div></div>
        <div class="col-sm-4"> <div id="piechart" ></div></div>
      </div>
      
    <!-- end chart -->



<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">

	  
	var analytics = <?php echo $sex; ?>

      google.charts.load('current', {'packages': ['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable(
          analytics
		);

        var options = {
          title: 'GENDER DISTRIBUTION',
          width: 550,
          height: 400,
          colors: ['#919190','#b9e1dc'],
          is3D: true
        };
        
        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }

    </script>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">
 google.charts.load("current", {packages:['corechart']});
 google.charts.setOnLoadCallback(drawChart);

 function drawChart() {
   var x = google.visualization.arrayToDataTable([
     ['Id', 'Amount', { role: 'style' } ],
     @foreach ($temps as $temp)
         ["{{ $temp['id'] }}", {{ $temp['amount'] }}, "#919190"],
     @endforeach
   ]);
   
   

   var view = new google.visualization.DataView(x);
   view.setColumns([0, 1,
                    { calc: "stringify",
                      sourceColumn: 1,
                      type: "string",
                      role: "annotation" },
                    2]);

   var options = {
     title: "NO. OF TENANTS PER MONTH",
     width: 700,
     height: 400,
     opacity: 0.2,
    
     bar: {groupWidth: "95%"},
     legend: { position: "none" }
     
   };
   var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
   chart.draw(view, options);
}

</script>

@endsection