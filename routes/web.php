<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
// // JERRROME

Route::get('david', 'DavidController@sms');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
    Route::get('viewmessage', 'MessageController@messages');
    Route::get('addmessage', 'MessageController@addmessage')->name('messages.index');
    Route::post('addmessage', 'MessageController@storemessage');
    Route::get('events2', 'EventsController@index')->name('events.index');
    Route::post('events2', 'EventsController@addEvent')->name('events.add');
});

Route::get('/', function () {
    return view('welcome');
});

Route::resource('/', 'GenInquiriesController');

Route::get('about/', 'AboutController@index');
Route::get('contactus/', 'GenInquiriesController@create');
Route::get('services/', 'ServicesController@index');

Route::get('/samplePDF/{id}','Voyager\TenantsController@samplePDF');
Route::get('/downloadPDF/{id}','Voyager\TenantsController@downloadPDF');


Route::get('/pdf', function(){
    return view('pdf');
});

Route::get('/sample/payment', function(){
    $tenant = \App\Tenant::find(16);

    
});