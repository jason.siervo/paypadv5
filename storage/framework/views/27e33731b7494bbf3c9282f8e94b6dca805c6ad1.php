<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title></title>
</head>
<body>
    <center>
        <h1>Billing Statement - <?php echo e($pdfPrint->bill_name); ?> </h1>

        <h3>University Pad Residences</h3>
        <h3>861 Padre Campa St., Sampaloc, Manila, 1008</h3>
        <br>
        <br>
    </center>

    <p><b>Name:</b> <?php echo e($pdfPrint->lastname); ?>, <?php echo e($pdfPrint->firstname); ?></p>
    <p><b>Billing Period:</b> <?php echo e($pdfPrint->start_date); ?> - <?php echo e($pdfPrint->end_date); ?> </p>
    <p><b>Due Date:</b> <?php echo e($pdfPrint->due_date); ?> </p>
    <br>
    <br>
    <p><b>Amount:</b> <?php echo e($pdfPrint->amount); ?></p>
    
    
</body>
</html>