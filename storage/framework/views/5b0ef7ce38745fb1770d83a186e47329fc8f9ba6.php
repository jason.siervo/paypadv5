<?php $__env->startPush('head'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('css/datatable.css')); ?>">
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>

    <div class="page-content read container-fluid">

    <a href="/admin/addmessage" class="pull-right btn btn-success">ADD</a>

    <table id="messagesTable">
        <thead>
            <tr>
                <th>From</th>
                <th style="max-width:300px">Message</th>
                <th>Time</th>
            </tr>
        </thead>
        <tbody>
            <?php $__currentLoopData = $messages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $message): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td><?php echo e($message->from); ?></td>
                    <td><?php echo e($message->message); ?></td>
                    <td><?php echo e($message->created_at); ?></td>
                </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
    </table>

</div>
<?php $__env->stopSection(); ?>

<script src="<?php echo e(asset('js/jquery.js')); ?>"></script>
<script src="<?php echo e(asset('js/datatable.js')); ?>"></script>

<?php $__env->startPush('after-scripts'); ?>
    <script>
        setTimeout(() => {
            $('#messagesTable').DataTable();
        }, 100);
    </script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('voyager::master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>