<style>
    body {font-family: Arial;}
    
    /* Style the tab */
    .tab {
        overflow: hidden;
        border: 1px solid #FFFFFF;
        background-color: #FFFFFF;
    }
    
    /* Style the buttons inside the tab */
    .tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
        font-size: 17px;
    }
    
    /* Change background color of buttons on hover */
    .tab button:hover {
        background-color: #ddd;
    }
    
    /* Create an active/current tablink class */
    .tab button.active {
        background-color: #ccc;
    }
    
    /* Style the tab content */
    .tabcontent {
        display: none;
        padding: 6px 12px;
        background-color: #FFFFFF;
        border: 1px solid #FFFFFF;
        border-top: none;
    }
</style>





<?php $__env->startSection('page_title', __('voyager::generic.view').' '.$dataType->display_name_singular); ?>

<?php $__env->startSection('page_header'); ?>
    <h1 class="page-title">
        <i class="<?php echo e($dataType->icon); ?>"></i> <?php echo e(__('voyager::generic.viewing')); ?> <?php echo e(ucfirst($dataType->display_name_singular)); ?> &nbsp;

        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', $dataTypeContent)): ?>
        <a href="<?php echo e(route('voyager.'.$dataType->slug.'.edit', $dataTypeContent->getKey())); ?>" class="btn btn-info">
            <span class="glyphicon glyphicon-pencil"></span>&nbsp;
            <?php echo e(__('voyager::generic.edit')); ?>

        </a>
        <?php endif; ?>
        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('delete', $dataTypeContent)): ?>
            <a href="javascript:;" title="<?php echo e(__('voyager::generic.delete')); ?>" class="btn btn-danger delete" data-id="<?php echo e($dataTypeContent->getKey()); ?>" id="delete-<?php echo e($dataTypeContent->getKey()); ?>">
                <i class="voyager-trash"></i> <span class="hidden-xs hidden-sm"><?php echo e(__('voyager::generic.delete')); ?></span>
            </a>
        <?php endif; ?>

        <a href="<?php echo e(route('voyager.'.$dataType->slug.'.index')); ?>" class="btn btn-warning">
            <span class="glyphicon glyphicon-list"></span>&nbsp;
            <?php echo e(__('voyager::generic.return_to_list')); ?>

        </a>
    </h1>
    <?php echo $__env->make('voyager::multilingual.language-selector', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="page-content read container-fluid">
        <div class="row">
            <div class="col-md-12">

                <?php $__currentLoopData = $dataType->readRows; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                <?php if($row->display_name == "Last Name"): ?>
                    <div class="col-md-12">
                        <br><h3 style="margin-left:-13px;">Personal Information</h3><br>
                    </div>
                <?php elseif($row->display_name == "Father's Name"): ?>
                    <div class="col-md-12">
                        <br><h3 style="margin-left:-13px;">Family Background</h3><br>
                    </div>
                <?php elseif($row->display_name == "Guardian's Name"): ?>
                    <div class="col-md-12">
                        <br><h3 style="margin-left:-13px;">Guardian's Information</h3><br>
                    </div>
                <?php elseif($row->display_name == "Emergency Contact Name"): ?>
                    <div class="col-md-12">
                        <br><h3 style="margin-left:-13px;">Person to call in case of emergency</h3><br>
                    </div>
                <?php elseif($row->display_name == "Signatory Name"): ?>
                    <div class="col-md-12">
                        <br><h3 style="margin-left:-13px;">Signatory of Contract</h3><br>
                    </div>
                <?php elseif($row->display_name == "Created At"): ?>
                    <div class="col-md-12">
                        <br><h3 style="margin-left:-13px;">Timestamps</h3><br>
                    </div>
                <?php endif; ?>

                <?php if($row->display_name == "Mother's Name"): ?>
                    <div class="col-md-12">
                        <br>
                    </div>
                <?php endif; ?>

                
                <div class="panel panel-bordered col-md-3" style="height: 100px;">

                    <!-- form start -->
                    
                    <?php $rowDetails = json_decode($row->details);
                    if($rowDetails === null){
                            $rowDetails=new stdClass();
                            $rowDetails->options=new stdClass();
                    }
                    ?>

                    <div class="panel-heading" style="border-bottom:0;">
                        <h3 class="panel-title"><?php echo e($row->display_name); ?></h3>
                    </div>

                    <div class="panel-body" style="padding-top:0;">
                        <?php if($row->type == "image"): ?>
                            <img class="img-responsive"
                                src="<?php echo e(filter_var($dataTypeContent->{$row->field}, FILTER_VALIDATE_URL) ? $dataTypeContent->{$row->field} : Voyager::image($dataTypeContent->{$row->field})); ?>">
                        <?php elseif($row->type == 'multiple_images'): ?>
                            <?php if(json_decode($dataTypeContent->{$row->field})): ?>
                                <?php $__currentLoopData = json_decode($dataTypeContent->{$row->field}); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $file): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <img class="img-responsive"
                                        src="<?php echo e(filter_var($file, FILTER_VALIDATE_URL) ? $file : Voyager::image($file)); ?>">
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php else: ?>
                                <img class="img-responsive"
                                    src="<?php echo e(filter_var($dataTypeContent->{$row->field}, FILTER_VALIDATE_URL) ? $dataTypeContent->{$row->field} : Voyager::image($dataTypeContent->{$row->field})); ?>">
                            <?php endif; ?>
                        <?php elseif($row->type == 'relationship'): ?>
                            <?php echo $__env->make('voyager::formfields.relationship', ['view' => 'read', 'options' => $rowDetails], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                        <?php elseif($row->type == 'select_dropdown' && property_exists($rowDetails, 'options') &&
                                !empty($rowDetails->options->{$dataTypeContent->{$row->field}})
                        ): ?>

                            <?php echo $rowDetails->options->{$dataTypeContent->{$row->field}};?>
                        <?php elseif($row->type == 'select_dropdown' && $dataTypeContent->{$row->field . '_page_slug'}): ?>
                            <a href="<?php echo e($dataTypeContent->{$row->field . '_page_slug'}); ?>"><?php echo e($dataTypeContent->{$row->field}); ?></a>
                        <?php elseif($row->type == 'select_multiple'): ?>
                            <?php if(property_exists($rowDetails, 'relationship')): ?>

                                <?php $__currentLoopData = json_decode($dataTypeContent->{$row->field}); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($item->{$row->field . '_page_slug'}): ?>
                                    <a href="<?php echo e($item->{$row->field . '_page_slug'}); ?>"><?php echo e($item->{$row->field}); ?></a><?php if(!$loop->last): ?>, <?php endif; ?>
                                    <?php else: ?>
                                    <?php echo e($item->{$row->field}); ?>

                                    <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                            <?php elseif(property_exists($rowDetails, 'options')): ?>
                                <?php $__currentLoopData = json_decode($dataTypeContent->{$row->field}); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php echo e($rowDetails->options->{$item} . (!$loop->last ? ', ' : '')); ?>

                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endif; ?>
                        <?php elseif($row->type == 'date' || $row->type == 'timestamp'): ?>
                            <?php echo e($rowDetails && property_exists($rowDetails, 'format') ? \Carbon\Carbon::parse($dataTypeContent->{$row->field})->formatLocalized($rowDetails->format) : $dataTypeContent->{$row->field}); ?>

                        <?php elseif($row->type == 'checkbox'): ?>
                            <?php if($rowDetails && property_exists($rowDetails, 'on') && property_exists($rowDetails, 'off')): ?>
                                <?php if($dataTypeContent->{$row->field}): ?>
                                <span class="label label-info"><?php echo e($rowDetails->on); ?></span>
                                <?php else: ?>
                                <span class="label label-primary"><?php echo e($rowDetails->off); ?></span>
                                <?php endif; ?>
                            <?php else: ?>
                            <?php echo e($dataTypeContent->{$row->field}); ?>

                            <?php endif; ?>
                        <?php elseif($row->type == 'color'): ?>
                            <span class="badge badge-lg" style="background-color: <?php echo e($dataTypeContent->{$row->field}); ?>"><?php echo e($dataTypeContent->{$row->field}); ?></span>
                        <?php elseif($row->type == 'coordinates'): ?>
                            <?php echo $__env->make('voyager::partials.coordinates', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                        <?php elseif($row->type == 'rich_text_box'): ?>
                            <?php echo $__env->make('voyager::multilingual.input-hidden-bread-read', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                            <p><?php echo $dataTypeContent->{$row->field}; ?></p>
                        <?php elseif($row->type == 'file'): ?>
                            <?php if(json_decode($dataTypeContent->{$row->field})): ?>
                                <?php $__currentLoopData = json_decode($dataTypeContent->{$row->field}); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $file): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <a href="<?php echo e(Storage::disk(config('voyager.storage.disk'))->url($file->download_link) ?: ''); ?>">
                                        <?php echo e($file->original_name ?: ''); ?>

                                    </a>
                                    <br/>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php else: ?>
                                <a href="<?php echo e(Storage::disk(config('voyager.storage.disk'))->url($row->field) ?: ''); ?>">
                                    <?php echo e(__('voyager::generic.download')); ?>

                                </a>
                            <?php endif; ?>
                        <?php else: ?>
                            <?php echo $__env->make('voyager::multilingual.input-hidden-bread-read', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                            <p><?php echo e($dataTypeContent->{$row->field}); ?></p>
                        <?php endif; ?>
                    </div><!-- panel-body -->
                    
                
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            </div>

            <div class="col-md-12">
                <div class="col-md-12">
                    <br><h3>Billing Information</h3><br>
                </div>
                <div class="tab">
                    <button class="tablinks" onclick="openCity(event, 'Billing')" id="defaultOpen"><b>Billing History</b></button>
                    
                </div>

                <div id="Billing" class="tabcontent">
                    <br>
                    <div class="table-responsive">
                        <table id="dataTable" class="table table-hover">
                            <thead>
                                <tr>
                                    <th><center>Bill Type</center></th>
                                    <th><center>Billing Date</center></th>
                                    <th><center>Amount Billed</center></th>
                                    <th><center>Actions</center></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $__currentLoopData = $bills; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bill): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td>
                                            <center>
                                                <?php echo e($bill->bill_name); ?>

                                            </center>
                                        </td>
                                        <td><center><?php echo e($bill->updated_at); ?></center></td>
                                        <td><center><?php echo e($bill->amount); ?></center></td>
                                        <td>
                                            <center>
                                                <div class="col-md-6">
                                                    <a href="<?php echo e(action('Voyager\TenantsController@samplePDF', $bill->id)); ?>"><span class="voyager-file-text"></span> View Bill</a>
                                                </div>
                                                <div class="col-md-6">
                                                    <a href="<?php echo e(action('Voyager\TenantsController@downloadPDF', $bill->id)); ?>"><span class="voyager-download"></span> Download Bill</a>
                                                </div>
                                            </center>
                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                
                <div id="Payment" class="tabcontent">
                    <br>
                    <div class="table-responsive">
                        <table id="dataTable" class="table table-hover">
                            <thead>
                                <tr>
                                    <th><center>Payment Date</center></th>
                                    <th><center>Amount Paid</center></th>
                                    <th><center>Payment Channel</center></th>
                                    <th><center>Actions</center></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><center>hello</center></td>
                                    <td><center>it's</center></td>
                                    <td><center>me</center></td>
                                    <td>
                                        <center>
                                            <a href="#"><span class="voyager-receipt"></span> Download Receipt</a>
                                        </center>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            </div>
        </div>
    </div>
    
    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="<?php echo e(__('voyager::generic.close')); ?>"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="voyager-trash"></i> <?php echo e(__('voyager::generic.delete_question')); ?> <?php echo e(strtolower($dataType->display_name_singular)); ?>?</h4>
                </div>
                <div class="modal-footer">
                    <form action="<?php echo e(route('voyager.'.$dataType->slug.'.index')); ?>" id="delete_form" method="POST">
                        <?php echo e(method_field("DELETE")); ?>

                        <?php echo e(csrf_field()); ?>

                        <input type="submit" class="btn btn-danger pull-right delete-confirm"
                               value="<?php echo e(__('voyager::generic.delete_confirm')); ?> <?php echo e(strtolower($dataType->display_name_singular)); ?>">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal"><?php echo e(__('voyager::generic.cancel')); ?></button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
    <?php if($isModelTranslatable): ?>
    <script>
        $(document).ready(function () {
            $('.side-body').multilingual();
        });
    </script>
    <script src="<?php echo e(voyager_asset('js/multilingual.js')); ?>"></script>
    <?php endif; ?>
    <script>
        var deleteFormAction;
        $('.delete').on('click', function (e) {
            var form = $('#delete_form')[0];

            if (!deleteFormAction) { // Save form action initial value
                deleteFormAction = form.action;
            }

            form.action = deleteFormAction.match(/\/[0-9]+$/)
                ? deleteFormAction.replace(/([0-9]+$)/, $(this).data('id'))
                : deleteFormAction + '/' + $(this).data('id');
            console.log(form.action);

            $('#delete_modal').modal('show');
        });

        function openCity(evt, cityName) {
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(cityName).style.display = "block";
            evt.currentTarget.className += " active";
        }

        // Get the element with id="defaultOpen" and click on it
        document.getElementById("defaultOpen").click();

    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('voyager::master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>