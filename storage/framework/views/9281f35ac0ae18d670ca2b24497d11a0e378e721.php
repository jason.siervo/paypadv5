<?php echo $__env->make('header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	
	

	<!-- <aside id="fh5co-hero" class="js-fullheight">
		<div class="flexslider js-fullheight">
			<ul class="slides">
		   	<li style="background-image: url(images/building1.png);">
		   		<div class="overlay-gradient"></div>
		   		<div class="container">
		   			<div class="col-md-10 col-md-offset-1 text-center js-fullheight slider-text">
		   				<div class="slider-text-inner"> -->
							 
						   <!-- <div id="fh5co-pricing-section" style="background-image: url(images/landscape.jpg);" >
		<div class="container">
			<div class="row"> 
			
				 <div class="col-md-6 col-md-offset-3 text-center fh5co-heading animate-box">
				<div class="price-box popular" style="background-image: url(images/blue1.gif);">
				<BR>
				
					</div>
				</div> 
			 </div>
</div></div> -->
		   					<!-- <p class="fh5co-lead">Designed with <i class="icon-heart"></i> by the fine folks at <a href="http://freehtml5.co" target="_blank">FreeHTML5.co</a></p> -->
		   				<!-- </div>
		   			</div>
		   		</div>
		   	</li>
		  	</ul>
	  	</div>
	</aside> -->



	<div class="fh5co-about animate-box">
		<div class="col-md-6 col-md-offset-3 text-center fh5co-heading">
			<h2>WHO. WE. ARE. </h2>
			<p><img src="images/summithill.png" "height="200" width="220"></p>

		</div>	
		<div class="container">
			
			<div class="col-md-12 animate-box">
				<figure><center>
				<iframe width="750" height="400" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Funiversitypadtaft%2Fvideos%2F1911368988908814%2F&show_text=0&width=560" width="560" height="315" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>
					<!-- <img src="images/building1.png" alt="Free HTML5 Template" class="img-responsive"> -->
			</center>	</figure>
			</div>
			<div class="col-md-8 col-md-push-2 animate-box">
				<h2>About our Company</h2>
				<p>University Pad Residences also called as Upad was established to become the trusted provider for parents looking for a dormitory that provides a conducive environments for studying, which is safe and secure residence. Upad provides first-class amenities with affordable rates so students who are both local and foreign that needs housing near universities can be accommodated. <br>
					
					<br>University Pad branches are strategically located near universities, malls and hospitals to help students easy access to basic necessities.
					Upad P. Ocampo branch is a dormitory in taft conveniently located in the heart of Metro Manila close to universities, malls, and hospitals.<br><br>
					
					This Upad Dormitory branch is near De Lasalle, St. Scholastica and near DLSU CSB.
					
					Upad P. Campa branch is a dormitory situated near the university belt in España Manila. Upad P. Campa Dormitory branch is near the following universities like UST, FEU, PSBA, UE, CEU, NU, CEU, STI and San Sebastian.</p>
				<!-- <p><a href="#" class="btn btn-primary btn-outline with-arrow">Learn more <i class="icon-arrow-right"></i></a></p> -->

			</div>
		</div>
	</div>

	<div class="fh5co-team fh5co-light-grey-section">
		<div class="container">
			
				<div class="row">
					<div class="col-md-6 col-md-offset-3 text-center fh5co-heading animate-box">
						<h2>The Team</h2>
						<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
					</div>
					<div class="col-md-4 fh5co-staff text-center animate-box">
						<img src="images/person1.jpg" alt="Free HTML5 Bootstrap Template" class="img-responsive">
						<h3>Athan Smith</h3>
						<h4>Co-Founder, CEO</h4>
						<p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar.</p>
						<ul class="fh5co-social">
							<li><a href="#"><i class="icon-google"></i></a></li>
							<li><a href="#"><i class="icon-dribbble"></i></a></li>
							<li><a href="#"><i class="icon-twitter"></i></a></li>
							<li><a href="#"><i class="icon-facebook"></i></a></li>
							<li><a href="#"><i class="icon-instagram"></i></a></li>
						</ul>
					</div>
					<div class="col-md-4 fh5co-staff text-center animate-box">
						<img src="images/person2.jpg" alt="Free HTML5 Bootstrap Template" class="img-responsive">
						<h3>Nathalie Kosley</h3>
						<h4>Co-Founder, CTO</h4>
						<p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar.</p>
						<ul class="fh5co-social">
							<li><a href="#"><i class="icon-google"></i></a></li>
							<li><a href="#"><i class="icon-dribbble"></i></a></li>
							<li><a href="#"><i class="icon-twitter"></i></a></li>
							<li><a href="#"><i class="icon-facebook"></i></a></li>
							<li><a href="#"><i class="icon-instagram"></i></a></li>
						</ul>
					</div>
					<div class="col-md-4 fh5co-staff text-center animate-box">
						<img src="images/person3.jpg" alt="Free HTML5 Bootstrap Template" class="img-responsive">
						<h3>Yanna Kuzuki</h3>
						<h4>Co-Founder, Principal</h4>
						<p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar.</p>
						<ul class="fh5co-social">
							<li><a href="#"><i class="icon-google"></i></a></li>
							<li><a href="#"><i class="icon-dribbble"></i></a></li>
							<li><a href="#"><i class="icon-twitter"></i></a></li>
							<li><a href="#"><i class="icon-facebook"></i></a></li>
							<li><a href="#"><i class="icon-instagram"></i></a></li>
						</ul>
					</div>
				</div>
		</div>
	</div>


	<div id="fh5co-services-section">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3 text-center fh5co-heading animate-box">
					<h2>Our pleasure to serve you</h2>
					<p>Upad is just an email or phone call away, 24/7. Contact us today.</p>
				</div>
				<div class="col-md-4 text-center item-block animate-box">
					<h3>Location</h3>
					<ul class="contact-info">
					<li><i class="icon-map"></i>P. Campa</li>
					<li>861 Padre Campa St, Sampaloc, Manila, 1008 Metro Manila</li>
					<li><i class="icon-map"></i>P. OCampa</li>
					<li>Ocampo St, Malate, Manila, 1004 Metro Manila</li>
						</ul>
				</div>
				<div class="col-md-4 text-center item-block animate-box">
					<h3>Phone numbers</h3>
					<ul class="contact-info">
					<li>
						<li><i class="icon-phone"></i>P.Campa Branch</li>
						<li>(+632) 7427485, (+632) 7427486, (+632) 7427491</li>
				
						<li>Mobile: 0917-7981437 , 0917-7981438 </li>
						
						
						</ul>

						
					<ul class="contact-info">
					<li>
						<li><i class="icon-phone"></i>P. OCampa Branch</li>
						<li>(+632) 4050104, (+632) 4050105, (+632) 7829995</li>
				
						<li>Mobile: 0916-4981833 </li>
						
						
						</ul>
				</div>
				<div class="col-md-4 text-center item-block animate-box">
					<h3>Website</h3>
					<ul class="contact-info">
					<li>
						<li><i class="icon-envelope"></i><a href="#">info_pcampa@universitypad.com.ph</a></li>
						<li><i class="icon-globe"></i><a href="#">www.universitypaypad.com</a></li>
						</ul>
				</div>
			</div>
		</div>
				


 <div id="fh5co-pricing-section">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3 text-center fh5co-heading animate-box">
					<h2>Mission</h2>
					<p>“To provide first-class amenities, secured environment, world- class service at an affordable value.”</p>
                </div>
                <div class="col-md-6 col-md-offset-3 text-center fh5co-heading animate-box">
					<h2>Vission</h2>
					<p>“To be the premium student residence in the Philippines” </p>
				</div>
				
			</div> </div></div>

	<div class="fh5co-cta" style="background-image: url(images/upad.jpg);">
		<div class="overlay"></div>
		<div class="container">
			<div class="col-md-12 text-center animate-box">
			<p><font size="60">SAFE. SECURE. CONVENIENT. AFFORDABLE.</font></p>

				<!-- <p><a href="#" class="btn btn-primary btn-outline with-arrow">Register now! <i class="icon-arrow-right"></i></a></p> -->
			</div>
		</div>
	</div>
	
	<?php echo $__env->make('footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>