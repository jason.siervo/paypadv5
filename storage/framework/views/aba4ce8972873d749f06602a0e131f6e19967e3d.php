<?php $__env->startSection('content'); ?>
dfsgsdgfdgfd
<div class="row">
    <div class="col-sm-4">
        <div id="piechart" style="width: 600px; height: 500px;"></div>
    </div>
    <div class="col-sm-4"> 
        <div id="columnchart_values" style="width: 900px; height: 300px;"></div>
    </div>
</div>


<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">
 google.charts.load("current", {packages:['corechart']});
 google.charts.setOnLoadCallback(drawChart);

 function drawChart() {
   var x = google.visualization.arrayToDataTable([
     ["Id", "Amount", { role: "style" } ],
     <?php $__currentLoopData = $temps; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $temp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
         ["<?php echo e($temp['id']); ?>", <?php echo e($temp['amount']); ?>, "#b87333"],
     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
   ]);
   
   

   var view = new google.visualization.DataView(x);
   view.setColumns([0, 1,
                    { calc: "stringify",
                      sourceColumn: 1,
                      type: "string",
                      role: "annotation" },
                    2]);

   var options = {
     title: "Tenants",
     width: 800,
     height: 400,
     bar: {groupWidth: "95%"},
     legend: { position: "none" },
   };
   var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
   chart.draw(view, options);
}

</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('voyager::master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>