<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillHistory extends Model
{
    private function isDuplicateEntryException(QueryException $e)
    {
        $sqlState = $e->errorInfo[0];
        $errorCode  = $e->errorInfo[1];
        if ($sqlState === "23000" && $errorCode === 1062) {
          return true;
        }
        return false;
    }

}
